library(tidyr)
library(dplyr)
library(reshape2)

### Download from https://portal.gdc.cancer.gov/repository
### Select CNV & copy number segment
### Select project
### Add all files to cart and navigate to cart
### Download zip file and biospecimen (TSV) into dir with project name (e.g. breast)
### Unzip gcd_download* into project folder
### Add project to projects vector
### Run script below

projects <- c('brain', 'breast', 'kidney', 'lung', 'ovary', 'prostate', 'uterus', 'colon')
data_dir <- "/its/home/gb293/phd/data/ploidy/ploidy_nmf/cnv_data"

raw_cnv_df <- data.frame(GDC_Aliquot=character(),	Chromosome=character(),	Start=integer(),	End=integer(),	Num_Probes=integer(),	Segment_Mean=numeric(), project_id=character(), sample_submitter_id=character())

for (project in projects) {
  wdir <- sprintf("%s/%s", data_dir, project)
  
  biospec <- list.files(wdir, pattern="biospec.*.tar.gz", full.names=TRUE, recursive = TRUE)[1]
  print(sprintf('untarring %s', biospec))
  untar(tarfile=biospec, exdir=sprintf('%s/%s', data_dir, project) )
  
  id_key <- read.table(sprintf('%s/%s/aliquot.tsv', data_dir, project), header=T) %>%
    select(aliquot_id, project_id, sample_submitter_id)
  cnv_files <- list.files(wdir, pattern="*seg.v2.txt$", full.names=TRUE, recursive = TRUE)
  
  sample_df <- data.frame(GDC_Aliquot=character(),	Chromosome=character(),	Start=integer(),	End=integer(),	Num_Probes=integer(),	Segment_Mean=numeric(), project_id=character(), sample_submitter_id=character())
  for (f in cnv_files) {
    print(f)
    sample_data <- read.table(f, header=T, fill = T)
    sample_df <- rbind(sample_df, sample_data)
  }
  
  sample_df <- sample_df %>% 
    left_join(id_key, by=c('GDC_Aliquot'='aliquot_id')) %>%
    distinct()
  
  raw_cnv_df <- rbind(raw_cnv_df, sample_df)
}

head(raw_cnv_df)

write.table(raw_cnv_df, sprintf('%s/processed_raw_cnv_data.csv', data_dir), sep=',')


