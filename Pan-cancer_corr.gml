Creator "igraph version 1.2.2 Mon Aug 19 16:10:12 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "c.1p"
  ]
  node
  [
    id 1
    name "c.2p"
  ]
  node
  [
    id 2
    name "c.2q"
  ]
  node
  [
    id 3
    name "c.3p"
  ]
  node
  [
    id 4
    name "c.3q"
  ]
  node
  [
    id 5
    name "c.4p"
  ]
  node
  [
    id 6
    name "c.4q"
  ]
  node
  [
    id 7
    name "c.5q"
  ]
  node
  [
    id 8
    name "c.6p"
  ]
  node
  [
    id 9
    name "c.6q"
  ]
  node
  [
    id 10
    name "c.7p"
  ]
  node
  [
    id 11
    name "c.7q"
  ]
  node
  [
    id 12
    name "c.8q"
  ]
  node
  [
    id 13
    name "c.10p"
  ]
  node
  [
    id 14
    name "c.10q"
  ]
  node
  [
    id 15
    name "c.11p"
  ]
  node
  [
    id 16
    name "c.11q"
  ]
  node
  [
    id 17
    name "c.15q"
  ]
  node
  [
    id 18
    name "c.16q"
  ]
  node
  [
    id 19
    name "c.17q"
  ]
  node
  [
    id 20
    name "c.19p"
  ]
  node
  [
    id 21
    name "c.22q"
  ]
  edge
  [
    source 0
    target 14
    weight 0.092
    weight2 -0.303
  ]
  edge
  [
    source 0
    target 15
    weight 0.092
    weight2 -0.303
  ]
  edge
  [
    source 0
    target 17
    weight 0.094
    weight2 -0.306
  ]
  edge
  [
    source 0
    target 18
    weight 0.094
    weight2 -0.306
  ]
  edge
  [
    source 0
    target 19
    weight 0.097
    weight2 -0.311
  ]
  edge
  [
    source 0
    target 21
    weight 0.117
    weight2 -0.341
  ]
  edge
  [
    source 1
    target 6
    weight 0.136
    weight2 -0.368
  ]
  edge
  [
    source 1
    target 7
    weight 0.155
    weight2 -0.393
  ]
  edge
  [
    source 2
    target 6
    weight 0.138
    weight2 -0.372
  ]
  edge
  [
    source 2
    target 7
    weight 0.105
    weight2 -0.324
  ]
  edge
  [
    source 3
    target 9
    weight 0.111
    weight2 -0.333
  ]
  edge
  [
    source 3
    target 18
    weight 0.101
    weight2 -0.317
  ]
  edge
  [
    source 3
    target 21
    weight 0.115
    weight2 -0.34
  ]
  edge
  [
    source 4
    target 6
    weight 0.156
    weight2 -0.394
  ]
  edge
  [
    source 4
    target 7
    weight 0.181
    weight2 -0.426
  ]
  edge
  [
    source 5
    target 14
    weight 0.093
    weight2 -0.305
  ]
  edge
  [
    source 6
    target 1
    weight 0.136
    weight2 -0.368
  ]
  edge
  [
    source 6
    target 2
    weight 0.138
    weight2 -0.372
  ]
  edge
  [
    source 6
    target 4
    weight 0.156
    weight2 -0.394
  ]
  edge
  [
    source 6
    target 8
    weight 0.103
    weight2 -0.32
  ]
  edge
  [
    source 6
    target 9
    weight 0.12
    weight2 -0.346
  ]
  edge
  [
    source 6
    target 13
    weight 0.092
    weight2 -0.304
  ]
  edge
  [
    source 6
    target 14
    weight 0.126
    weight2 -0.356
  ]
  edge
  [
    source 7
    target 1
    weight 0.155
    weight2 -0.393
  ]
  edge
  [
    source 7
    target 2
    weight 0.105
    weight2 -0.324
  ]
  edge
  [
    source 7
    target 4
    weight 0.181
    weight2 -0.426
  ]
  edge
  [
    source 7
    target 8
    weight 0.145
    weight2 -0.38
  ]
  edge
  [
    source 7
    target 9
    weight 0.102
    weight2 -0.319
  ]
  edge
  [
    source 8
    target 6
    weight 0.103
    weight2 -0.32
  ]
  edge
  [
    source 8
    target 7
    weight 0.145
    weight2 -0.38
  ]
  edge
  [
    source 9
    target 3
    weight 0.111
    weight2 -0.333
  ]
  edge
  [
    source 9
    target 6
    weight 0.12
    weight2 -0.346
  ]
  edge
  [
    source 9
    target 7
    weight 0.102
    weight2 -0.319
  ]
  edge
  [
    source 10
    target 12
    weight 0.117
    weight2 -0.342
  ]
  edge
  [
    source 10
    target 13
    weight 0.162
    weight2 -0.402
  ]
  edge
  [
    source 10
    target 14
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 11
    target 12
    weight 0.094
    weight2 -0.307
  ]
  edge
  [
    source 11
    target 13
    weight 0.128
    weight2 -0.358
  ]
  edge
  [
    source 11
    target 14
    weight 0.155
    weight2 -0.393
  ]
  edge
  [
    source 12
    target 10
    weight 0.117
    weight2 -0.342
  ]
  edge
  [
    source 12
    target 11
    weight 0.094
    weight2 -0.307
  ]
  edge
  [
    source 13
    target 6
    weight 0.092
    weight2 -0.304
  ]
  edge
  [
    source 13
    target 10
    weight 0.162
    weight2 -0.402
  ]
  edge
  [
    source 13
    target 11
    weight 0.128
    weight2 -0.358
  ]
  edge
  [
    source 13
    target 20
    weight 0.1
    weight2 -0.316
  ]
  edge
  [
    source 14
    target 0
    weight 0.092
    weight2 -0.303
  ]
  edge
  [
    source 14
    target 5
    weight 0.093
    weight2 -0.305
  ]
  edge
  [
    source 14
    target 6
    weight 0.126
    weight2 -0.356
  ]
  edge
  [
    source 14
    target 10
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 14
    target 11
    weight 0.155
    weight2 -0.393
  ]
  edge
  [
    source 14
    target 20
    weight 0.135
    weight2 -0.368
  ]
  edge
  [
    source 15
    target 0
    weight 0.092
    weight2 -0.303
  ]
  edge
  [
    source 16
    target 19
    weight 0.106
    weight2 -0.325
  ]
  edge
  [
    source 17
    target 0
    weight 0.094
    weight2 -0.306
  ]
  edge
  [
    source 18
    target 0
    weight 0.094
    weight2 -0.306
  ]
  edge
  [
    source 18
    target 3
    weight 0.101
    weight2 -0.317
  ]
  edge
  [
    source 19
    target 0
    weight 0.097
    weight2 -0.311
  ]
  edge
  [
    source 19
    target 16
    weight 0.106
    weight2 -0.325
  ]
  edge
  [
    source 20
    target 13
    weight 0.1
    weight2 -0.316
  ]
  edge
  [
    source 20
    target 14
    weight 0.135
    weight2 -0.368
  ]
  edge
  [
    source 21
    target 0
    weight 0.117
    weight2 -0.341
  ]
  edge
  [
    source 21
    target 3
    weight 0.115
    weight2 -0.34
  ]
]
