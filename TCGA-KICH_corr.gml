Creator "igraph version 1.2.2 Wed Jul 24 12:20:26 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "c.1p"
  ]
  node
  [
    id 1
    name "c.1q"
  ]
  node
  [
    id 2
    name "c.2p"
  ]
  node
  [
    id 3
    name "c.2q"
  ]
  node
  [
    id 4
    name "c.3p"
  ]
  node
  [
    id 5
    name "c.3q"
  ]
  node
  [
    id 6
    name "c.4p"
  ]
  node
  [
    id 7
    name "c.4q"
  ]
  node
  [
    id 8
    name "c.5p"
  ]
  node
  [
    id 9
    name "c.5q"
  ]
  node
  [
    id 10
    name "c.6p"
  ]
  node
  [
    id 11
    name "c.6q"
  ]
  node
  [
    id 12
    name "c.7p"
  ]
  node
  [
    id 13
    name "c.7q"
  ]
  node
  [
    id 14
    name "c.8p"
  ]
  node
  [
    id 15
    name "c.8q"
  ]
  node
  [
    id 16
    name "c.9p"
  ]
  node
  [
    id 17
    name "c.9q"
  ]
  node
  [
    id 18
    name "c.10p"
  ]
  node
  [
    id 19
    name "c.10q"
  ]
  node
  [
    id 20
    name "c.11p"
  ]
  node
  [
    id 21
    name "c.11q"
  ]
  node
  [
    id 22
    name "c.12p"
  ]
  node
  [
    id 23
    name "c.12q"
  ]
  node
  [
    id 24
    name "c.13q"
  ]
  node
  [
    id 25
    name "c.14q"
  ]
  node
  [
    id 26
    name "c.15q"
  ]
  node
  [
    id 27
    name "c.16p"
  ]
  node
  [
    id 28
    name "c.16q"
  ]
  node
  [
    id 29
    name "c.17p"
  ]
  node
  [
    id 30
    name "c.17q"
  ]
  node
  [
    id 31
    name "c.18p"
  ]
  node
  [
    id 32
    name "c.18q"
  ]
  node
  [
    id 33
    name "c.19p"
  ]
  node
  [
    id 34
    name "c.19q"
  ]
  node
  [
    id 35
    name "c.20p"
  ]
  node
  [
    id 36
    name "c.20q"
  ]
  node
  [
    id 37
    name "c.21q"
  ]
  node
  [
    id 38
    name "c.22q"
  ]
  edge
  [
    source 0
    target 2
    weight 0.17
    weight2 -0.413
  ]
  edge
  [
    source 0
    target 10
    weight 0.169
    weight2 -0.411
  ]
  edge
  [
    source 0
    target 11
    weight 0.185
    weight2 -0.43
  ]
  edge
  [
    source 0
    target 19
    weight 0.586
    weight2 -0.765
  ]
  edge
  [
    source 0
    target 23
    weight 0.318
    weight2 -0.564
  ]
  edge
  [
    source 0
    target 24
    weight 0.31
    weight2 -0.557
  ]
  edge
  [
    source 0
    target 28
    weight 0.244
    weight2 -0.494
  ]
  edge
  [
    source 0
    target 29
    weight 0.228
    weight2 -0.478
  ]
  edge
  [
    source 0
    target 30
    weight 0.198
    weight2 -0.445
  ]
  edge
  [
    source 0
    target 33
    weight 0.171
    weight2 -0.413
  ]
  edge
  [
    source 0
    target 37
    weight 0.264
    weight2 -0.514
  ]
  edge
  [
    source 1
    target 4
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 1
    target 5
    weight 0.464
    weight2 -0.681
  ]
  edge
  [
    source 1
    target 23
    weight 0.178
    weight2 -0.421
  ]
  edge
  [
    source 1
    target 24
    weight 0.234
    weight2 -0.484
  ]
  edge
  [
    source 1
    target 34
    weight 0.253
    weight2 -0.503
  ]
  edge
  [
    source 1
    target 37
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 2
    target 0
    weight 0.17
    weight2 -0.413
  ]
  edge
  [
    source 2
    target 3
    weight 0.944
    weight2 0.972
  ]
  edge
  [
    source 2
    target 4
    weight 0.58
    weight2 -0.762
  ]
  edge
  [
    source 2
    target 5
    weight 0.463
    weight2 -0.68
  ]
  edge
  [
    source 2
    target 9
    weight 0.316
    weight2 -0.562
  ]
  edge
  [
    source 2
    target 12
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 2
    target 13
    weight 0.174
    weight2 -0.417
  ]
  edge
  [
    source 2
    target 16
    weight 0.607
    weight2 -0.779
  ]
  edge
  [
    source 2
    target 17
    weight 0.544
    weight2 -0.737
  ]
  edge
  [
    source 2
    target 24
    weight 0.38
    weight2 -0.616
  ]
  edge
  [
    source 2
    target 25
    weight 0.292
    weight2 -0.541
  ]
  edge
  [
    source 2
    target 26
    weight 0.544
    weight2 -0.738
  ]
  edge
  [
    source 2
    target 33
    weight 0.194
    weight2 -0.44
  ]
  edge
  [
    source 2
    target 34
    weight 0.373
    weight2 -0.611
  ]
  edge
  [
    source 2
    target 37
    weight 0.376
    weight2 -0.613
  ]
  edge
  [
    source 3
    target 2
    weight 0.944
    weight2 0.972
  ]
  edge
  [
    source 3
    target 4
    weight 0.734
    weight2 -0.856
  ]
  edge
  [
    source 3
    target 5
    weight 0.679
    weight2 -0.824
  ]
  edge
  [
    source 3
    target 12
    weight 0.211
    weight2 -0.459
  ]
  edge
  [
    source 3
    target 13
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 3
    target 16
    weight 0.519
    weight2 -0.72
  ]
  edge
  [
    source 3
    target 17
    weight 0.516
    weight2 -0.718
  ]
  edge
  [
    source 3
    target 25
    weight 0.369
    weight2 -0.608
  ]
  edge
  [
    source 3
    target 26
    weight 0.426
    weight2 -0.653
  ]
  edge
  [
    source 3
    target 33
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 3
    target 34
    weight 0.317
    weight2 -0.563
  ]
  edge
  [
    source 4
    target 1
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 4
    target 2
    weight 0.58
    weight2 -0.762
  ]
  edge
  [
    source 4
    target 3
    weight 0.734
    weight2 -0.856
  ]
  edge
  [
    source 4
    target 7
    weight 0.224
    weight2 -0.474
  ]
  edge
  [
    source 4
    target 8
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 4
    target 10
    weight 0.358
    weight2 -0.598
  ]
  edge
  [
    source 4
    target 11
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 4
    target 14
    weight 0.212
    weight2 -0.46
  ]
  edge
  [
    source 4
    target 15
    weight 0.21
    weight2 -0.458
  ]
  edge
  [
    source 4
    target 16
    weight 0.19
    weight2 -0.436
  ]
  edge
  [
    source 4
    target 18
    weight 0.53
    weight2 -0.728
  ]
  edge
  [
    source 4
    target 19
    weight 0.622
    weight2 -0.789
  ]
  edge
  [
    source 4
    target 20
    weight 0.243
    weight2 -0.493
  ]
  edge
  [
    source 4
    target 21
    weight 0.344
    weight2 -0.586
  ]
  edge
  [
    source 4
    target 24
    weight 0.324
    weight2 -0.57
  ]
  edge
  [
    source 4
    target 27
    weight 0.238
    weight2 -0.488
  ]
  edge
  [
    source 4
    target 28
    weight 0.482
    weight2 -0.694
  ]
  edge
  [
    source 4
    target 30
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 4
    target 31
    weight 0.21
    weight2 -0.459
  ]
  edge
  [
    source 4
    target 32
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 4
    target 37
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 5
    target 1
    weight 0.464
    weight2 -0.681
  ]
  edge
  [
    source 5
    target 2
    weight 0.463
    weight2 -0.68
  ]
  edge
  [
    source 5
    target 3
    weight 0.679
    weight2 -0.824
  ]
  edge
  [
    source 5
    target 7
    weight 0.323
    weight2 -0.568
  ]
  edge
  [
    source 5
    target 8
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 5
    target 9
    weight 0.292
    weight2 -0.54
  ]
  edge
  [
    source 5
    target 10
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 5
    target 11
    weight 0.27
    weight2 -0.519
  ]
  edge
  [
    source 5
    target 12
    weight 0.295
    weight2 -0.543
  ]
  edge
  [
    source 5
    target 14
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 5
    target 15
    weight 0.312
    weight2 -0.558
  ]
  edge
  [
    source 5
    target 16
    weight 0.218
    weight2 -0.466
  ]
  edge
  [
    source 5
    target 18
    weight 0.544
    weight2 -0.737
  ]
  edge
  [
    source 5
    target 19
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 5
    target 20
    weight 0.513
    weight2 -0.716
  ]
  edge
  [
    source 5
    target 21
    weight 0.584
    weight2 -0.764
  ]
  edge
  [
    source 5
    target 23
    weight 0.217
    weight2 -0.466
  ]
  edge
  [
    source 5
    target 24
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 5
    target 25
    weight 0.204
    weight2 -0.452
  ]
  edge
  [
    source 5
    target 27
    weight 0.353
    weight2 -0.594
  ]
  edge
  [
    source 5
    target 28
    weight 0.41
    weight2 -0.64
  ]
  edge
  [
    source 5
    target 31
    weight 0.3
    weight2 -0.548
  ]
  edge
  [
    source 5
    target 32
    weight 0.419
    weight2 -0.647
  ]
  edge
  [
    source 5
    target 34
    weight 0.162
    weight2 -0.403
  ]
  edge
  [
    source 5
    target 36
    weight 0.272
    weight2 -0.522
  ]
  edge
  [
    source 5
    target 37
    weight 0.414
    weight2 -0.643
  ]
  edge
  [
    source 6
    target 10
    weight 0.326
    weight2 -0.571
  ]
  edge
  [
    source 6
    target 26
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 7
    target 4
    weight 0.224
    weight2 -0.474
  ]
  edge
  [
    source 7
    target 5
    weight 0.323
    weight2 -0.568
  ]
  edge
  [
    source 7
    target 8
    weight 0.244
    weight2 -0.493
  ]
  edge
  [
    source 7
    target 9
    weight 0.183
    weight2 -0.428
  ]
  edge
  [
    source 7
    target 10
    weight 0.279
    weight2 -0.528
  ]
  edge
  [
    source 7
    target 27
    weight 0.316
    weight2 0.562
  ]
  edge
  [
    source 7
    target 28
    weight 0.592
    weight2 0.769
  ]
  edge
  [
    source 7
    target 29
    weight 0.182
    weight2 -0.427
  ]
  edge
  [
    source 8
    target 4
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 8
    target 5
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 8
    target 7
    weight 0.244
    weight2 -0.493
  ]
  edge
  [
    source 8
    target 9
    weight 0.294
    weight2 0.542
  ]
  edge
  [
    source 8
    target 14
    weight 0.327
    weight2 -0.572
  ]
  edge
  [
    source 8
    target 15
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 8
    target 16
    weight 0.309
    weight2 -0.555
  ]
  edge
  [
    source 8
    target 18
    weight 0.423
    weight2 -0.65
  ]
  edge
  [
    source 8
    target 19
    weight 0.409
    weight2 -0.64
  ]
  edge
  [
    source 8
    target 20
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 8
    target 21
    weight 0.231
    weight2 -0.481
  ]
  edge
  [
    source 8
    target 23
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 8
    target 27
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 8
    target 28
    weight 0.387
    weight2 -0.622
  ]
  edge
  [
    source 8
    target 31
    weight 0.164
    weight2 -0.405
  ]
  edge
  [
    source 8
    target 32
    weight 0.232
    weight2 -0.482
  ]
  edge
  [
    source 8
    target 37
    weight 0.347
    weight2 -0.589
  ]
  edge
  [
    source 9
    target 2
    weight 0.316
    weight2 -0.562
  ]
  edge
  [
    source 9
    target 5
    weight 0.292
    weight2 -0.54
  ]
  edge
  [
    source 9
    target 7
    weight 0.183
    weight2 -0.428
  ]
  edge
  [
    source 9
    target 8
    weight 0.294
    weight2 0.542
  ]
  edge
  [
    source 9
    target 10
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 9
    target 11
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 9
    target 14
    weight 0.494
    weight2 -0.703
  ]
  edge
  [
    source 9
    target 15
    weight 0.231
    weight2 -0.481
  ]
  edge
  [
    source 9
    target 16
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 9
    target 18
    weight 0.528
    weight2 -0.726
  ]
  edge
  [
    source 9
    target 19
    weight 0.407
    weight2 -0.638
  ]
  edge
  [
    source 9
    target 20
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 9
    target 21
    weight 0.289
    weight2 -0.537
  ]
  edge
  [
    source 9
    target 27
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 9
    target 32
    weight 0.388
    weight2 -0.623
  ]
  edge
  [
    source 9
    target 35
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 9
    target 37
    weight 0.348
    weight2 -0.59
  ]
  edge
  [
    source 10
    target 0
    weight 0.169
    weight2 -0.411
  ]
  edge
  [
    source 10
    target 4
    weight 0.358
    weight2 -0.598
  ]
  edge
  [
    source 10
    target 5
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 10
    target 6
    weight 0.326
    weight2 -0.571
  ]
  edge
  [
    source 10
    target 7
    weight 0.279
    weight2 -0.528
  ]
  edge
  [
    source 10
    target 9
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 10
    target 11
    weight 0.877
    weight2 0.937
  ]
  edge
  [
    source 10
    target 16
    weight 0.352
    weight2 -0.593
  ]
  edge
  [
    source 10
    target 17
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 10
    target 23
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 10
    target 25
    weight 0.312
    weight2 -0.559
  ]
  edge
  [
    source 10
    target 26
    weight 0.262
    weight2 -0.512
  ]
  edge
  [
    source 10
    target 27
    weight 0.298
    weight2 -0.546
  ]
  edge
  [
    source 10
    target 28
    weight 0.17
    weight2 -0.413
  ]
  edge
  [
    source 10
    target 29
    weight 0.458
    weight2 0.677
  ]
  edge
  [
    source 10
    target 30
    weight 0.444
    weight2 0.666
  ]
  edge
  [
    source 10
    target 33
    weight 0.272
    weight2 -0.522
  ]
  edge
  [
    source 10
    target 34
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 10
    target 37
    weight 0.357
    weight2 -0.598
  ]
  edge
  [
    source 11
    target 0
    weight 0.185
    weight2 -0.43
  ]
  edge
  [
    source 11
    target 4
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 11
    target 5
    weight 0.27
    weight2 -0.519
  ]
  edge
  [
    source 11
    target 9
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 11
    target 10
    weight 0.877
    weight2 0.937
  ]
  edge
  [
    source 11
    target 16
    weight 0.296
    weight2 -0.544
  ]
  edge
  [
    source 11
    target 17
    weight 0.205
    weight2 -0.453
  ]
  edge
  [
    source 11
    target 25
    weight 0.204
    weight2 -0.452
  ]
  edge
  [
    source 11
    target 29
    weight 0.282
    weight2 0.531
  ]
  edge
  [
    source 11
    target 30
    weight 0.311
    weight2 0.558
  ]
  edge
  [
    source 11
    target 33
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 12
    target 2
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 12
    target 3
    weight 0.211
    weight2 -0.459
  ]
  edge
  [
    source 12
    target 5
    weight 0.295
    weight2 -0.543
  ]
  edge
  [
    source 12
    target 13
    weight 0.626
    weight2 0.791
  ]
  edge
  [
    source 12
    target 14
    weight 0.278
    weight2 -0.528
  ]
  edge
  [
    source 12
    target 23
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 12
    target 31
    weight 0.232
    weight2 -0.482
  ]
  edge
  [
    source 12
    target 35
    weight 0.203
    weight2 -0.451
  ]
  edge
  [
    source 13
    target 2
    weight 0.174
    weight2 -0.417
  ]
  edge
  [
    source 13
    target 3
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 13
    target 12
    weight 0.626
    weight2 0.791
  ]
  edge
  [
    source 13
    target 31
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 14
    target 4
    weight 0.212
    weight2 -0.46
  ]
  edge
  [
    source 14
    target 5
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 14
    target 8
    weight 0.327
    weight2 -0.572
  ]
  edge
  [
    source 14
    target 9
    weight 0.494
    weight2 -0.703
  ]
  edge
  [
    source 14
    target 12
    weight 0.278
    weight2 -0.528
  ]
  edge
  [
    source 14
    target 16
    weight 0.291
    weight2 -0.539
  ]
  edge
  [
    source 14
    target 17
    weight 0.334
    weight2 -0.578
  ]
  edge
  [
    source 14
    target 18
    weight 0.199
    weight2 -0.447
  ]
  edge
  [
    source 14
    target 20
    weight 0.405
    weight2 -0.637
  ]
  edge
  [
    source 14
    target 21
    weight 0.393
    weight2 -0.627
  ]
  edge
  [
    source 14
    target 22
    weight 0.162
    weight2 -0.403
  ]
  edge
  [
    source 14
    target 23
    weight 0.527
    weight2 -0.726
  ]
  edge
  [
    source 14
    target 24
    weight 0.507
    weight2 -0.712
  ]
  edge
  [
    source 14
    target 26
    weight 0.318
    weight2 -0.564
  ]
  edge
  [
    source 14
    target 27
    weight 0.312
    weight2 -0.558
  ]
  edge
  [
    source 14
    target 28
    weight 0.44
    weight2 -0.663
  ]
  edge
  [
    source 14
    target 32
    weight 0.438
    weight2 -0.662
  ]
  edge
  [
    source 14
    target 34
    weight 0.293
    weight2 -0.541
  ]
  edge
  [
    source 14
    target 36
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 14
    target 37
    weight 0.518
    weight2 -0.72
  ]
  edge
  [
    source 14
    target 38
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 15
    target 4
    weight 0.21
    weight2 -0.458
  ]
  edge
  [
    source 15
    target 5
    weight 0.312
    weight2 -0.558
  ]
  edge
  [
    source 15
    target 8
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 15
    target 9
    weight 0.231
    weight2 -0.481
  ]
  edge
  [
    source 15
    target 16
    weight 0.347
    weight2 -0.589
  ]
  edge
  [
    source 15
    target 17
    weight 0.203
    weight2 -0.451
  ]
  edge
  [
    source 15
    target 20
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 15
    target 21
    weight 0.189
    weight2 -0.435
  ]
  edge
  [
    source 15
    target 23
    weight 0.169
    weight2 -0.411
  ]
  edge
  [
    source 15
    target 24
    weight 0.423
    weight2 -0.651
  ]
  edge
  [
    source 15
    target 26
    weight 0.174
    weight2 -0.418
  ]
  edge
  [
    source 15
    target 28
    weight 0.192
    weight2 -0.439
  ]
  edge
  [
    source 15
    target 32
    weight 0.2
    weight2 -0.447
  ]
  edge
  [
    source 15
    target 36
    weight 0.199
    weight2 -0.446
  ]
  edge
  [
    source 15
    target 37
    weight 0.34
    weight2 -0.583
  ]
  edge
  [
    source 16
    target 2
    weight 0.607
    weight2 -0.779
  ]
  edge
  [
    source 16
    target 3
    weight 0.519
    weight2 -0.72
  ]
  edge
  [
    source 16
    target 4
    weight 0.19
    weight2 -0.436
  ]
  edge
  [
    source 16
    target 5
    weight 0.218
    weight2 -0.466
  ]
  edge
  [
    source 16
    target 8
    weight 0.309
    weight2 -0.555
  ]
  edge
  [
    source 16
    target 9
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 16
    target 10
    weight 0.352
    weight2 -0.593
  ]
  edge
  [
    source 16
    target 11
    weight 0.296
    weight2 -0.544
  ]
  edge
  [
    source 16
    target 14
    weight 0.291
    weight2 -0.539
  ]
  edge
  [
    source 16
    target 15
    weight 0.347
    weight2 -0.589
  ]
  edge
  [
    source 16
    target 17
    weight 0.345
    weight2 0.587
  ]
  edge
  [
    source 16
    target 18
    weight 0.527
    weight2 -0.726
  ]
  edge
  [
    source 16
    target 19
    weight 0.562
    weight2 -0.75
  ]
  edge
  [
    source 16
    target 20
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 16
    target 21
    weight 0.271
    weight2 -0.52
  ]
  edge
  [
    source 16
    target 22
    weight 0.297
    weight2 -0.545
  ]
  edge
  [
    source 16
    target 23
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 16
    target 24
    weight 0.442
    weight2 -0.665
  ]
  edge
  [
    source 16
    target 27
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 16
    target 28
    weight 0.426
    weight2 -0.653
  ]
  edge
  [
    source 16
    target 29
    weight 0.278
    weight2 -0.527
  ]
  edge
  [
    source 16
    target 30
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 16
    target 31
    weight 0.225
    weight2 -0.474
  ]
  edge
  [
    source 16
    target 32
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 16
    target 36
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 16
    target 37
    weight 0.277
    weight2 -0.526
  ]
  edge
  [
    source 17
    target 2
    weight 0.544
    weight2 -0.737
  ]
  edge
  [
    source 17
    target 3
    weight 0.516
    weight2 -0.718
  ]
  edge
  [
    source 17
    target 10
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 17
    target 11
    weight 0.205
    weight2 -0.453
  ]
  edge
  [
    source 17
    target 14
    weight 0.334
    weight2 -0.578
  ]
  edge
  [
    source 17
    target 15
    weight 0.203
    weight2 -0.451
  ]
  edge
  [
    source 17
    target 16
    weight 0.345
    weight2 0.587
  ]
  edge
  [
    source 17
    target 18
    weight 0.217
    weight2 -0.466
  ]
  edge
  [
    source 17
    target 19
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 17
    target 24
    weight 0.283
    weight2 -0.532
  ]
  edge
  [
    source 17
    target 28
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 17
    target 29
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 17
    target 30
    weight 0.305
    weight2 -0.553
  ]
  edge
  [
    source 17
    target 32
    weight 0.185
    weight2 -0.43
  ]
  edge
  [
    source 18
    target 4
    weight 0.53
    weight2 -0.728
  ]
  edge
  [
    source 18
    target 5
    weight 0.544
    weight2 -0.737
  ]
  edge
  [
    source 18
    target 8
    weight 0.423
    weight2 -0.65
  ]
  edge
  [
    source 18
    target 9
    weight 0.528
    weight2 -0.726
  ]
  edge
  [
    source 18
    target 14
    weight 0.199
    weight2 -0.447
  ]
  edge
  [
    source 18
    target 16
    weight 0.527
    weight2 -0.726
  ]
  edge
  [
    source 18
    target 17
    weight 0.217
    weight2 -0.466
  ]
  edge
  [
    source 18
    target 19
    weight 0.645
    weight2 0.803
  ]
  edge
  [
    source 18
    target 21
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 18
    target 24
    weight 0.21
    weight2 -0.459
  ]
  edge
  [
    source 18
    target 25
    weight 0.256
    weight2 -0.506
  ]
  edge
  [
    source 18
    target 26
    weight 0.184
    weight2 -0.429
  ]
  edge
  [
    source 18
    target 37
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 19
    target 0
    weight 0.586
    weight2 -0.765
  ]
  edge
  [
    source 19
    target 4
    weight 0.622
    weight2 -0.789
  ]
  edge
  [
    source 19
    target 5
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 19
    target 8
    weight 0.409
    weight2 -0.64
  ]
  edge
  [
    source 19
    target 9
    weight 0.407
    weight2 -0.638
  ]
  edge
  [
    source 19
    target 16
    weight 0.562
    weight2 -0.75
  ]
  edge
  [
    source 19
    target 17
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 19
    target 18
    weight 0.645
    weight2 0.803
  ]
  edge
  [
    source 19
    target 25
    weight 0.236
    weight2 -0.486
  ]
  edge
  [
    source 19
    target 26
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 20
    target 4
    weight 0.243
    weight2 -0.493
  ]
  edge
  [
    source 20
    target 5
    weight 0.513
    weight2 -0.716
  ]
  edge
  [
    source 20
    target 8
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 20
    target 9
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 20
    target 14
    weight 0.405
    weight2 -0.637
  ]
  edge
  [
    source 20
    target 15
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 20
    target 16
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 20
    target 22
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 20
    target 23
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 20
    target 24
    weight 0.188
    weight2 -0.433
  ]
  edge
  [
    source 20
    target 26
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 20
    target 27
    weight 0.243
    weight2 -0.493
  ]
  edge
  [
    source 20
    target 28
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 20
    target 31
    weight 0.244
    weight2 -0.494
  ]
  edge
  [
    source 20
    target 32
    weight 0.215
    weight2 -0.464
  ]
  edge
  [
    source 20
    target 34
    weight 0.264
    weight2 -0.514
  ]
  edge
  [
    source 20
    target 37
    weight 0.239
    weight2 -0.489
  ]
  edge
  [
    source 21
    target 4
    weight 0.344
    weight2 -0.586
  ]
  edge
  [
    source 21
    target 5
    weight 0.584
    weight2 -0.764
  ]
  edge
  [
    source 21
    target 8
    weight 0.231
    weight2 -0.481
  ]
  edge
  [
    source 21
    target 9
    weight 0.289
    weight2 -0.537
  ]
  edge
  [
    source 21
    target 14
    weight 0.393
    weight2 -0.627
  ]
  edge
  [
    source 21
    target 15
    weight 0.189
    weight2 -0.435
  ]
  edge
  [
    source 21
    target 16
    weight 0.271
    weight2 -0.52
  ]
  edge
  [
    source 21
    target 18
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 21
    target 22
    weight 0.246
    weight2 -0.496
  ]
  edge
  [
    source 21
    target 23
    weight 0.251
    weight2 -0.501
  ]
  edge
  [
    source 21
    target 24
    weight 0.198
    weight2 -0.445
  ]
  edge
  [
    source 21
    target 25
    weight 0.222
    weight2 -0.471
  ]
  edge
  [
    source 21
    target 26
    weight 0.369
    weight2 -0.608
  ]
  edge
  [
    source 21
    target 27
    weight 0.324
    weight2 -0.57
  ]
  edge
  [
    source 21
    target 31
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 21
    target 32
    weight 0.189
    weight2 -0.435
  ]
  edge
  [
    source 21
    target 33
    weight 0.19
    weight2 -0.435
  ]
  edge
  [
    source 21
    target 35
    weight 0.355
    weight2 -0.596
  ]
  edge
  [
    source 21
    target 36
    weight 0.361
    weight2 -0.601
  ]
  edge
  [
    source 21
    target 37
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 22
    target 14
    weight 0.162
    weight2 -0.403
  ]
  edge
  [
    source 22
    target 16
    weight 0.297
    weight2 -0.545
  ]
  edge
  [
    source 22
    target 20
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 22
    target 21
    weight 0.246
    weight2 -0.496
  ]
  edge
  [
    source 22
    target 37
    weight 0.174
    weight2 -0.417
  ]
  edge
  [
    source 23
    target 0
    weight 0.318
    weight2 -0.564
  ]
  edge
  [
    source 23
    target 1
    weight 0.178
    weight2 -0.421
  ]
  edge
  [
    source 23
    target 5
    weight 0.217
    weight2 -0.466
  ]
  edge
  [
    source 23
    target 8
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 23
    target 10
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 23
    target 12
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 23
    target 14
    weight 0.527
    weight2 -0.726
  ]
  edge
  [
    source 23
    target 15
    weight 0.169
    weight2 -0.411
  ]
  edge
  [
    source 23
    target 16
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 23
    target 20
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 23
    target 21
    weight 0.251
    weight2 -0.501
  ]
  edge
  [
    source 23
    target 29
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 23
    target 30
    weight 0.182
    weight2 -0.426
  ]
  edge
  [
    source 23
    target 36
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 23
    target 37
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 24
    target 0
    weight 0.31
    weight2 -0.557
  ]
  edge
  [
    source 24
    target 1
    weight 0.234
    weight2 -0.484
  ]
  edge
  [
    source 24
    target 2
    weight 0.38
    weight2 -0.616
  ]
  edge
  [
    source 24
    target 4
    weight 0.324
    weight2 -0.57
  ]
  edge
  [
    source 24
    target 5
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 24
    target 14
    weight 0.507
    weight2 -0.712
  ]
  edge
  [
    source 24
    target 15
    weight 0.423
    weight2 -0.651
  ]
  edge
  [
    source 24
    target 16
    weight 0.442
    weight2 -0.665
  ]
  edge
  [
    source 24
    target 17
    weight 0.283
    weight2 -0.532
  ]
  edge
  [
    source 24
    target 18
    weight 0.21
    weight2 -0.459
  ]
  edge
  [
    source 24
    target 20
    weight 0.188
    weight2 -0.433
  ]
  edge
  [
    source 24
    target 21
    weight 0.198
    weight2 -0.445
  ]
  edge
  [
    source 24
    target 37
    weight 0.337
    weight2 -0.581
  ]
  edge
  [
    source 25
    target 2
    weight 0.292
    weight2 -0.541
  ]
  edge
  [
    source 25
    target 3
    weight 0.369
    weight2 -0.608
  ]
  edge
  [
    source 25
    target 5
    weight 0.204
    weight2 -0.452
  ]
  edge
  [
    source 25
    target 10
    weight 0.312
    weight2 -0.559
  ]
  edge
  [
    source 25
    target 11
    weight 0.204
    weight2 -0.452
  ]
  edge
  [
    source 25
    target 18
    weight 0.256
    weight2 -0.506
  ]
  edge
  [
    source 25
    target 19
    weight 0.236
    weight2 -0.486
  ]
  edge
  [
    source 25
    target 21
    weight 0.222
    weight2 -0.471
  ]
  edge
  [
    source 25
    target 26
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 25
    target 29
    weight 0.269
    weight2 -0.519
  ]
  edge
  [
    source 25
    target 30
    weight 0.371
    weight2 -0.609
  ]
  edge
  [
    source 25
    target 31
    weight 0.2
    weight2 -0.448
  ]
  edge
  [
    source 26
    target 2
    weight 0.544
    weight2 -0.738
  ]
  edge
  [
    source 26
    target 3
    weight 0.426
    weight2 -0.653
  ]
  edge
  [
    source 26
    target 6
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 26
    target 10
    weight 0.262
    weight2 -0.512
  ]
  edge
  [
    source 26
    target 14
    weight 0.318
    weight2 -0.564
  ]
  edge
  [
    source 26
    target 15
    weight 0.174
    weight2 -0.418
  ]
  edge
  [
    source 26
    target 18
    weight 0.184
    weight2 -0.429
  ]
  edge
  [
    source 26
    target 19
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 26
    target 20
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 26
    target 21
    weight 0.369
    weight2 -0.608
  ]
  edge
  [
    source 26
    target 25
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 26
    target 27
    weight 0.257
    weight2 -0.507
  ]
  edge
  [
    source 26
    target 28
    weight 0.446
    weight2 -0.667
  ]
  edge
  [
    source 26
    target 29
    weight 0.191
    weight2 -0.438
  ]
  edge
  [
    source 26
    target 31
    weight 0.411
    weight2 -0.641
  ]
  edge
  [
    source 26
    target 32
    weight 0.209
    weight2 -0.458
  ]
  edge
  [
    source 26
    target 36
    weight 0.246
    weight2 -0.496
  ]
  edge
  [
    source 27
    target 4
    weight 0.238
    weight2 -0.488
  ]
  edge
  [
    source 27
    target 5
    weight 0.353
    weight2 -0.594
  ]
  edge
  [
    source 27
    target 7
    weight 0.316
    weight2 0.562
  ]
  edge
  [
    source 27
    target 8
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 27
    target 9
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 27
    target 10
    weight 0.298
    weight2 -0.546
  ]
  edge
  [
    source 27
    target 14
    weight 0.312
    weight2 -0.558
  ]
  edge
  [
    source 27
    target 16
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 27
    target 20
    weight 0.243
    weight2 -0.493
  ]
  edge
  [
    source 27
    target 21
    weight 0.324
    weight2 -0.57
  ]
  edge
  [
    source 27
    target 26
    weight 0.257
    weight2 -0.507
  ]
  edge
  [
    source 27
    target 29
    weight 0.294
    weight2 -0.542
  ]
  edge
  [
    source 27
    target 30
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 27
    target 33
    weight 0.176
    weight2 -0.42
  ]
  edge
  [
    source 27
    target 36
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 28
    target 0
    weight 0.244
    weight2 -0.494
  ]
  edge
  [
    source 28
    target 4
    weight 0.482
    weight2 -0.694
  ]
  edge
  [
    source 28
    target 5
    weight 0.41
    weight2 -0.64
  ]
  edge
  [
    source 28
    target 7
    weight 0.592
    weight2 0.769
  ]
  edge
  [
    source 28
    target 8
    weight 0.387
    weight2 -0.622
  ]
  edge
  [
    source 28
    target 10
    weight 0.17
    weight2 -0.413
  ]
  edge
  [
    source 28
    target 14
    weight 0.44
    weight2 -0.663
  ]
  edge
  [
    source 28
    target 15
    weight 0.192
    weight2 -0.439
  ]
  edge
  [
    source 28
    target 16
    weight 0.426
    weight2 -0.653
  ]
  edge
  [
    source 28
    target 17
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 28
    target 20
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 28
    target 26
    weight 0.446
    weight2 -0.667
  ]
  edge
  [
    source 28
    target 29
    weight 0.233
    weight2 -0.482
  ]
  edge
  [
    source 28
    target 30
    weight 0.287
    weight2 -0.536
  ]
  edge
  [
    source 28
    target 33
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 28
    target 34
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 28
    target 36
    weight 0.253
    weight2 -0.503
  ]
  edge
  [
    source 29
    target 0
    weight 0.228
    weight2 -0.478
  ]
  edge
  [
    source 29
    target 7
    weight 0.182
    weight2 -0.427
  ]
  edge
  [
    source 29
    target 10
    weight 0.458
    weight2 0.677
  ]
  edge
  [
    source 29
    target 11
    weight 0.282
    weight2 0.531
  ]
  edge
  [
    source 29
    target 16
    weight 0.278
    weight2 -0.527
  ]
  edge
  [
    source 29
    target 17
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 29
    target 23
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 29
    target 25
    weight 0.269
    weight2 -0.519
  ]
  edge
  [
    source 29
    target 26
    weight 0.191
    weight2 -0.438
  ]
  edge
  [
    source 29
    target 27
    weight 0.294
    weight2 -0.542
  ]
  edge
  [
    source 29
    target 28
    weight 0.233
    weight2 -0.482
  ]
  edge
  [
    source 29
    target 30
    weight 0.828
    weight2 0.91
  ]
  edge
  [
    source 29
    target 33
    weight 0.324
    weight2 -0.569
  ]
  edge
  [
    source 29
    target 34
    weight 0.23
    weight2 -0.479
  ]
  edge
  [
    source 29
    target 37
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 30
    target 0
    weight 0.198
    weight2 -0.445
  ]
  edge
  [
    source 30
    target 4
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 30
    target 10
    weight 0.444
    weight2 0.666
  ]
  edge
  [
    source 30
    target 11
    weight 0.311
    weight2 0.558
  ]
  edge
  [
    source 30
    target 16
    weight 0.45
    weight2 -0.671
  ]
  edge
  [
    source 30
    target 17
    weight 0.305
    weight2 -0.553
  ]
  edge
  [
    source 30
    target 23
    weight 0.182
    weight2 -0.426
  ]
  edge
  [
    source 30
    target 25
    weight 0.371
    weight2 -0.609
  ]
  edge
  [
    source 30
    target 27
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 30
    target 28
    weight 0.287
    weight2 -0.536
  ]
  edge
  [
    source 30
    target 29
    weight 0.828
    weight2 0.91
  ]
  edge
  [
    source 30
    target 33
    weight 0.261
    weight2 -0.511
  ]
  edge
  [
    source 30
    target 34
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 30
    target 37
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 31
    target 4
    weight 0.21
    weight2 -0.459
  ]
  edge
  [
    source 31
    target 5
    weight 0.3
    weight2 -0.548
  ]
  edge
  [
    source 31
    target 8
    weight 0.164
    weight2 -0.405
  ]
  edge
  [
    source 31
    target 12
    weight 0.232
    weight2 -0.482
  ]
  edge
  [
    source 31
    target 13
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 31
    target 16
    weight 0.225
    weight2 -0.474
  ]
  edge
  [
    source 31
    target 20
    weight 0.244
    weight2 -0.494
  ]
  edge
  [
    source 31
    target 21
    weight 0.235
    weight2 -0.485
  ]
  edge
  [
    source 31
    target 25
    weight 0.2
    weight2 -0.448
  ]
  edge
  [
    source 31
    target 26
    weight 0.411
    weight2 -0.641
  ]
  edge
  [
    source 31
    target 35
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 31
    target 37
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 32
    target 4
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 32
    target 5
    weight 0.419
    weight2 -0.647
  ]
  edge
  [
    source 32
    target 8
    weight 0.232
    weight2 -0.482
  ]
  edge
  [
    source 32
    target 9
    weight 0.388
    weight2 -0.623
  ]
  edge
  [
    source 32
    target 14
    weight 0.438
    weight2 -0.662
  ]
  edge
  [
    source 32
    target 15
    weight 0.2
    weight2 -0.447
  ]
  edge
  [
    source 32
    target 16
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 32
    target 17
    weight 0.185
    weight2 -0.43
  ]
  edge
  [
    source 32
    target 20
    weight 0.215
    weight2 -0.464
  ]
  edge
  [
    source 32
    target 21
    weight 0.189
    weight2 -0.435
  ]
  edge
  [
    source 32
    target 26
    weight 0.209
    weight2 -0.458
  ]
  edge
  [
    source 32
    target 35
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 33
    target 0
    weight 0.171
    weight2 -0.413
  ]
  edge
  [
    source 33
    target 2
    weight 0.194
    weight2 -0.44
  ]
  edge
  [
    source 33
    target 3
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 33
    target 10
    weight 0.272
    weight2 -0.522
  ]
  edge
  [
    source 33
    target 11
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 33
    target 21
    weight 0.19
    weight2 -0.435
  ]
  edge
  [
    source 33
    target 27
    weight 0.176
    weight2 -0.42
  ]
  edge
  [
    source 33
    target 28
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 33
    target 29
    weight 0.324
    weight2 -0.569
  ]
  edge
  [
    source 33
    target 30
    weight 0.261
    weight2 -0.511
  ]
  edge
  [
    source 33
    target 34
    weight 0.367
    weight2 -0.606
  ]
  edge
  [
    source 34
    target 1
    weight 0.253
    weight2 -0.503
  ]
  edge
  [
    source 34
    target 2
    weight 0.373
    weight2 -0.611
  ]
  edge
  [
    source 34
    target 3
    weight 0.317
    weight2 -0.563
  ]
  edge
  [
    source 34
    target 5
    weight 0.162
    weight2 -0.403
  ]
  edge
  [
    source 34
    target 10
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 34
    target 14
    weight 0.293
    weight2 -0.541
  ]
  edge
  [
    source 34
    target 20
    weight 0.264
    weight2 -0.514
  ]
  edge
  [
    source 34
    target 28
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 34
    target 29
    weight 0.23
    weight2 -0.479
  ]
  edge
  [
    source 34
    target 30
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 34
    target 33
    weight 0.367
    weight2 -0.606
  ]
  edge
  [
    source 34
    target 36
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 34
    target 38
    weight 0.261
    weight2 -0.511
  ]
  edge
  [
    source 35
    target 9
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 35
    target 12
    weight 0.203
    weight2 -0.451
  ]
  edge
  [
    source 35
    target 21
    weight 0.355
    weight2 -0.596
  ]
  edge
  [
    source 35
    target 31
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 35
    target 32
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 35
    target 37
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 36
    target 5
    weight 0.272
    weight2 -0.522
  ]
  edge
  [
    source 36
    target 14
    weight 0.173
    weight2 -0.416
  ]
  edge
  [
    source 36
    target 15
    weight 0.199
    weight2 -0.446
  ]
  edge
  [
    source 36
    target 16
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 36
    target 21
    weight 0.361
    weight2 -0.601
  ]
  edge
  [
    source 36
    target 23
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 36
    target 26
    weight 0.246
    weight2 -0.496
  ]
  edge
  [
    source 36
    target 27
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 36
    target 28
    weight 0.253
    weight2 -0.503
  ]
  edge
  [
    source 36
    target 34
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 37
    target 0
    weight 0.264
    weight2 -0.514
  ]
  edge
  [
    source 37
    target 1
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 37
    target 2
    weight 0.376
    weight2 -0.613
  ]
  edge
  [
    source 37
    target 4
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 37
    target 5
    weight 0.414
    weight2 -0.643
  ]
  edge
  [
    source 37
    target 8
    weight 0.347
    weight2 -0.589
  ]
  edge
  [
    source 37
    target 9
    weight 0.348
    weight2 -0.59
  ]
  edge
  [
    source 37
    target 10
    weight 0.357
    weight2 -0.598
  ]
  edge
  [
    source 37
    target 14
    weight 0.518
    weight2 -0.72
  ]
  edge
  [
    source 37
    target 15
    weight 0.34
    weight2 -0.583
  ]
  edge
  [
    source 37
    target 16
    weight 0.277
    weight2 -0.526
  ]
  edge
  [
    source 37
    target 18
    weight 0.242
    weight2 -0.492
  ]
  edge
  [
    source 37
    target 20
    weight 0.239
    weight2 -0.489
  ]
  edge
  [
    source 37
    target 21
    weight 0.233
    weight2 -0.483
  ]
  edge
  [
    source 37
    target 22
    weight 0.174
    weight2 -0.417
  ]
  edge
  [
    source 37
    target 23
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 37
    target 24
    weight 0.337
    weight2 -0.581
  ]
  edge
  [
    source 37
    target 29
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 37
    target 30
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 37
    target 31
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 37
    target 35
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 38
    target 14
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 38
    target 34
    weight 0.261
    weight2 -0.511
  ]
]
