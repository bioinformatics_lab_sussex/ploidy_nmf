Creator "igraph version 1.2.2 Wed Apr 10 10:00:49 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "c.1p"
  ]
  node
  [
    id 1
    name "c.1q"
  ]
  node
  [
    id 2
    name "c.2p"
  ]
  node
  [
    id 3
    name "c.2q"
  ]
  node
  [
    id 4
    name "c.3p"
  ]
  node
  [
    id 5
    name "c.3q"
  ]
  node
  [
    id 6
    name "c.4p"
  ]
  node
  [
    id 7
    name "c.4q"
  ]
  node
  [
    id 8
    name "c.5p"
  ]
  node
  [
    id 9
    name "c.5q"
  ]
  node
  [
    id 10
    name "c.6p"
  ]
  node
  [
    id 11
    name "c.6q"
  ]
  node
  [
    id 12
    name "c.7p"
  ]
  node
  [
    id 13
    name "c.7q"
  ]
  node
  [
    id 14
    name "c.8p"
  ]
  node
  [
    id 15
    name "c.8q"
  ]
  node
  [
    id 16
    name "c.9p"
  ]
  node
  [
    id 17
    name "c.9q"
  ]
  node
  [
    id 18
    name "c.10p"
  ]
  node
  [
    id 19
    name "c.10q"
  ]
  node
  [
    id 20
    name "c.11p"
  ]
  node
  [
    id 21
    name "c.11q"
  ]
  node
  [
    id 22
    name "c.12p"
  ]
  node
  [
    id 23
    name "c.12q"
  ]
  node
  [
    id 24
    name "c.13q"
  ]
  node
  [
    id 25
    name "c.14q"
  ]
  node
  [
    id 26
    name "c.15q"
  ]
  node
  [
    id 27
    name "c.16p"
  ]
  node
  [
    id 28
    name "c.16q"
  ]
  node
  [
    id 29
    name "c.17p"
  ]
  node
  [
    id 30
    name "c.17q"
  ]
  node
  [
    id 31
    name "c.18p"
  ]
  node
  [
    id 32
    name "c.18q"
  ]
  node
  [
    id 33
    name "c.19p"
  ]
  node
  [
    id 34
    name "c.19q"
  ]
  node
  [
    id 35
    name "c.20p"
  ]
  node
  [
    id 36
    name "c.20q"
  ]
  node
  [
    id 37
    name "c.21p"
  ]
  node
  [
    id 38
    name "c.21q"
  ]
  node
  [
    id 39
    name "c.22q"
  ]
  edge
  [
    source 0
    target 0
    weight 0
  ]
  edge
  [
    source 0
    target 1
    weight 0.004
  ]
  edge
  [
    source 0
    target 2
    weight 0.097
  ]
  edge
  [
    source 0
    target 3
    weight 0.019
  ]
  edge
  [
    source 0
    target 4
    weight 0.005
  ]
  edge
  [
    source 0
    target 5
    weight 0.028
  ]
  edge
  [
    source 0
    target 6
    weight 0.001
  ]
  edge
  [
    source 0
    target 7
    weight 0.001
  ]
  edge
  [
    source 0
    target 8
    weight 0.029
  ]
  edge
  [
    source 0
    target 9
    weight 0
  ]
  edge
  [
    source 0
    target 10
    weight 0.164
  ]
  edge
  [
    source 0
    target 11
    weight 0.14
  ]
  edge
  [
    source 0
    target 12
    weight 0.025
  ]
  edge
  [
    source 0
    target 13
    weight 0.005
  ]
  edge
  [
    source 0
    target 14
    weight 0.001
  ]
  edge
  [
    source 0
    target 15
    weight 0
  ]
  edge
  [
    source 0
    target 16
    weight 0.06
  ]
  edge
  [
    source 0
    target 17
    weight 0.151
  ]
  edge
  [
    source 0
    target 18
    weight 0.074
  ]
  edge
  [
    source 0
    target 19
    weight 0.572
  ]
  edge
  [
    source 0
    target 20
    weight 0.002
  ]
  edge
  [
    source 0
    target 21
    weight 0.051
  ]
  edge
  [
    source 0
    target 22
    weight 0.01
  ]
  edge
  [
    source 0
    target 23
    weight 0.288
  ]
  edge
  [
    source 0
    target 24
    weight 0.312
  ]
  edge
  [
    source 0
    target 25
    weight 0.002
  ]
  edge
  [
    source 0
    target 26
    weight 0.002
  ]
  edge
  [
    source 0
    target 27
    weight 0.006
  ]
  edge
  [
    source 0
    target 28
    weight 0.231
  ]
  edge
  [
    source 0
    target 29
    weight 0.149
  ]
  edge
  [
    source 0
    target 30
    weight 0.05
  ]
  edge
  [
    source 0
    target 31
    weight 0.051
  ]
  edge
  [
    source 0
    target 32
    weight 0.001
  ]
  edge
  [
    source 0
    target 33
    weight 0.166
  ]
  edge
  [
    source 0
    target 34
    weight 0.095
  ]
  edge
  [
    source 0
    target 35
    weight 0.046
  ]
  edge
  [
    source 0
    target 36
    weight 0.044
  ]
  edge
  [
    source 0
    target 37
    weight 0.009
  ]
  edge
  [
    source 0
    target 38
    weight 0.104
  ]
  edge
  [
    source 0
    target 39
    weight 0
  ]
  edge
  [
    source 1
    target 0
    weight 0.004
  ]
  edge
  [
    source 1
    target 1
    weight 0
  ]
  edge
  [
    source 1
    target 2
    weight 0.009
  ]
  edge
  [
    source 1
    target 3
    weight 0.174
  ]
  edge
  [
    source 1
    target 4
    weight 0.411
  ]
  edge
  [
    source 1
    target 5
    weight 0.426
  ]
  edge
  [
    source 1
    target 6
    weight 0.01
  ]
  edge
  [
    source 1
    target 7
    weight 0.019
  ]
  edge
  [
    source 1
    target 8
    weight 0.004
  ]
  edge
  [
    source 1
    target 9
    weight 0.005
  ]
  edge
  [
    source 1
    target 10
    weight 0.028
  ]
  edge
  [
    source 1
    target 11
    weight 0.011
  ]
  edge
  [
    source 1
    target 12
    weight 0.035
  ]
  edge
  [
    source 1
    target 13
    weight 0.03
  ]
  edge
  [
    source 1
    target 14
    weight 0.164
  ]
  edge
  [
    source 1
    target 15
    weight 0.069
  ]
  edge
  [
    source 1
    target 16
    weight 0.144
  ]
  edge
  [
    source 1
    target 17
    weight 0.071
  ]
  edge
  [
    source 1
    target 18
    weight 0.034
  ]
  edge
  [
    source 1
    target 19
    weight 0.003
  ]
  edge
  [
    source 1
    target 20
    weight 0.063
  ]
  edge
  [
    source 1
    target 21
    weight 0.026
  ]
  edge
  [
    source 1
    target 22
    weight 0.033
  ]
  edge
  [
    source 1
    target 23
    weight 0.138
  ]
  edge
  [
    source 1
    target 24
    weight 0.2
  ]
  edge
  [
    source 1
    target 25
    weight 0.091
  ]
  edge
  [
    source 1
    target 26
    weight 0.042
  ]
  edge
  [
    source 1
    target 27
    weight 0.075
  ]
  edge
  [
    source 1
    target 28
    weight 0.033
  ]
  edge
  [
    source 1
    target 29
    weight 0.167
  ]
  edge
  [
    source 1
    target 30
    weight 0.001
  ]
  edge
  [
    source 1
    target 31
    weight 0.012
  ]
  edge
  [
    source 1
    target 32
    weight 0.018
  ]
  edge
  [
    source 1
    target 33
    weight 0.194
  ]
  edge
  [
    source 1
    target 34
    weight 0.252
  ]
  edge
  [
    source 1
    target 35
    weight 0
  ]
  edge
  [
    source 1
    target 36
    weight 0.002
  ]
  edge
  [
    source 1
    target 37
    weight 0.019
  ]
  edge
  [
    source 1
    target 38
    weight 0.173
  ]
  edge
  [
    source 1
    target 39
    weight 0.009
  ]
  edge
  [
    source 2
    target 0
    weight 0.097
  ]
  edge
  [
    source 2
    target 1
    weight 0.009
  ]
  edge
  [
    source 2
    target 2
    weight 0
  ]
  edge
  [
    source 2
    target 3
    weight 0.823
  ]
  edge
  [
    source 2
    target 4
    weight 0.545
  ]
  edge
  [
    source 2
    target 5
    weight 0.422
  ]
  edge
  [
    source 2
    target 6
    weight 0.012
  ]
  edge
  [
    source 2
    target 7
    weight 0.015
  ]
  edge
  [
    source 2
    target 8
    weight 0.108
  ]
  edge
  [
    source 2
    target 9
    weight 0.295
  ]
  edge
  [
    source 2
    target 10
    weight 0.008
  ]
  edge
  [
    source 2
    target 11
    weight 0.004
  ]
  edge
  [
    source 2
    target 12
    weight 0.157
  ]
  edge
  [
    source 2
    target 13
    weight 0.188
  ]
  edge
  [
    source 2
    target 14
    weight 0
  ]
  edge
  [
    source 2
    target 15
    weight 0.008
  ]
  edge
  [
    source 2
    target 16
    weight 0.579
  ]
  edge
  [
    source 2
    target 17
    weight 0.55
  ]
  edge
  [
    source 2
    target 18
    weight 0.006
  ]
  edge
  [
    source 2
    target 19
    weight 0.009
  ]
  edge
  [
    source 2
    target 20
    weight 0.113
  ]
  edge
  [
    source 2
    target 21
    weight 0.076
  ]
  edge
  [
    source 2
    target 22
    weight 0.056
  ]
  edge
  [
    source 2
    target 23
    weight 0.121
  ]
  edge
  [
    source 2
    target 24
    weight 0.32
  ]
  edge
  [
    source 2
    target 25
    weight 0.308
  ]
  edge
  [
    source 2
    target 26
    weight 0.564
  ]
  edge
  [
    source 2
    target 27
    weight 0.105
  ]
  edge
  [
    source 2
    target 28
    weight 0.047
  ]
  edge
  [
    source 2
    target 29
    weight 0.103
  ]
  edge
  [
    source 2
    target 30
    weight 0
  ]
  edge
  [
    source 2
    target 31
    weight 0.002
  ]
  edge
  [
    source 2
    target 32
    weight 0.028
  ]
  edge
  [
    source 2
    target 33
    weight 0.12
  ]
  edge
  [
    source 2
    target 34
    weight 0.211
  ]
  edge
  [
    source 2
    target 35
    weight 0.002
  ]
  edge
  [
    source 2
    target 36
    weight 0.015
  ]
  edge
  [
    source 2
    target 37
    weight 0.011
  ]
  edge
  [
    source 2
    target 38
    weight 0.451
  ]
  edge
  [
    source 2
    target 39
    weight 0.06
  ]
  edge
  [
    source 3
    target 0
    weight 0.019
  ]
  edge
  [
    source 3
    target 1
    weight 0.174
  ]
  edge
  [
    source 3
    target 2
    weight 0.823
  ]
  edge
  [
    source 3
    target 3
    weight 0
  ]
  edge
  [
    source 3
    target 4
    weight 0.706
  ]
  edge
  [
    source 3
    target 5
    weight 0.685
  ]
  edge
  [
    source 3
    target 6
    weight 0.003
  ]
  edge
  [
    source 3
    target 7
    weight 0.001
  ]
  edge
  [
    source 3
    target 8
    weight 0.092
  ]
  edge
  [
    source 3
    target 9
    weight 0.21
  ]
  edge
  [
    source 3
    target 10
    weight 0.121
  ]
  edge
  [
    source 3
    target 11
    weight 0.084
  ]
  edge
  [
    source 3
    target 12
    weight 0.211
  ]
  edge
  [
    source 3
    target 13
    weight 0.259
  ]
  edge
  [
    source 3
    target 14
    weight 0.011
  ]
  edge
  [
    source 3
    target 15
    weight 0.001
  ]
  edge
  [
    source 3
    target 16
    weight 0.455
  ]
  edge
  [
    source 3
    target 17
    weight 0.462
  ]
  edge
  [
    source 3
    target 18
    weight 0.058
  ]
  edge
  [
    source 3
    target 19
    weight 0.047
  ]
  edge
  [
    source 3
    target 20
    weight 0.134
  ]
  edge
  [
    source 3
    target 21
    weight 0.069
  ]
  edge
  [
    source 3
    target 22
    weight 0.084
  ]
  edge
  [
    source 3
    target 23
    weight 0.082
  ]
  edge
  [
    source 3
    target 24
    weight 0.057
  ]
  edge
  [
    source 3
    target 25
    weight 0.379
  ]
  edge
  [
    source 3
    target 26
    weight 0.463
  ]
  edge
  [
    source 3
    target 27
    weight 0.102
  ]
  edge
  [
    source 3
    target 28
    weight 0.003
  ]
  edge
  [
    source 3
    target 29
    weight 0.232
  ]
  edge
  [
    source 3
    target 30
    weight 0.067
  ]
  edge
  [
    source 3
    target 31
    weight 0
  ]
  edge
  [
    source 3
    target 32
    weight 0.034
  ]
  edge
  [
    source 3
    target 33
    weight 0.141
  ]
  edge
  [
    source 3
    target 34
    weight 0.404
  ]
  edge
  [
    source 3
    target 35
    weight 0.034
  ]
  edge
  [
    source 3
    target 36
    weight 0.022
  ]
  edge
  [
    source 3
    target 37
    weight 0.012
  ]
  edge
  [
    source 3
    target 38
    weight 0.11
  ]
  edge
  [
    source 3
    target 39
    weight 0.081
  ]
  edge
  [
    source 4
    target 0
    weight 0.005
  ]
  edge
  [
    source 4
    target 1
    weight 0.411
  ]
  edge
  [
    source 4
    target 2
    weight 0.545
  ]
  edge
  [
    source 4
    target 3
    weight 0.706
  ]
  edge
  [
    source 4
    target 4
    weight 0
  ]
  edge
  [
    source 4
    target 5
    weight 0.175
  ]
  edge
  [
    source 4
    target 6
    weight 0.049
  ]
  edge
  [
    source 4
    target 7
    weight 0.21
  ]
  edge
  [
    source 4
    target 8
    weight 0.069
  ]
  edge
  [
    source 4
    target 9
    weight 0.069
  ]
  edge
  [
    source 4
    target 10
    weight 0.332
  ]
  edge
  [
    source 4
    target 11
    weight 0.163
  ]
  edge
  [
    source 4
    target 12
    weight 0.053
  ]
  edge
  [
    source 4
    target 13
    weight 0.005
  ]
  edge
  [
    source 4
    target 14
    weight 0.082
  ]
  edge
  [
    source 4
    target 15
    weight 0.129
  ]
  edge
  [
    source 4
    target 16
    weight 0.025
  ]
  edge
  [
    source 4
    target 17
    weight 0
  ]
  edge
  [
    source 4
    target 18
    weight 0.463
  ]
  edge
  [
    source 4
    target 19
    weight 0.566
  ]
  edge
  [
    source 4
    target 20
    weight 0.213
  ]
  edge
  [
    source 4
    target 21
    weight 0.328
  ]
  edge
  [
    source 4
    target 22
    weight 0.016
  ]
  edge
  [
    source 4
    target 23
    weight 0.028
  ]
  edge
  [
    source 4
    target 24
    weight 0.284
  ]
  edge
  [
    source 4
    target 25
    weight 0.031
  ]
  edge
  [
    source 4
    target 26
    weight 0.058
  ]
  edge
  [
    source 4
    target 27
    weight 0.139
  ]
  edge
  [
    source 4
    target 28
    weight 0.143
  ]
  edge
  [
    source 4
    target 29
    weight 0.142
  ]
  edge
  [
    source 4
    target 30
    weight 0.23
  ]
  edge
  [
    source 4
    target 31
    weight 0.211
  ]
  edge
  [
    source 4
    target 32
    weight 0.195
  ]
  edge
  [
    source 4
    target 33
    weight 0.009
  ]
  edge
  [
    source 4
    target 34
    weight 0.019
  ]
  edge
  [
    source 4
    target 35
    weight 0.009
  ]
  edge
  [
    source 4
    target 36
    weight 0.12
  ]
  edge
  [
    source 4
    target 37
    weight 0.005
  ]
  edge
  [
    source 4
    target 38
    weight 0.335
  ]
  edge
  [
    source 4
    target 39
    weight 0.011
  ]
  edge
  [
    source 5
    target 0
    weight 0.028
  ]
  edge
  [
    source 5
    target 1
    weight 0.426
  ]
  edge
  [
    source 5
    target 2
    weight 0.422
  ]
  edge
  [
    source 5
    target 3
    weight 0.685
  ]
  edge
  [
    source 5
    target 4
    weight 0.175
  ]
  edge
  [
    source 5
    target 5
    weight 0
  ]
  edge
  [
    source 5
    target 6
    weight 0.14
  ]
  edge
  [
    source 5
    target 7
    weight 0.402
  ]
  edge
  [
    source 5
    target 8
    weight 0.109
  ]
  edge
  [
    source 5
    target 9
    weight 0.255
  ]
  edge
  [
    source 5
    target 10
    weight 0.245
  ]
  edge
  [
    source 5
    target 11
    weight 0.251
  ]
  edge
  [
    source 5
    target 12
    weight 0.213
  ]
  edge
  [
    source 5
    target 13
    weight 0.064
  ]
  edge
  [
    source 5
    target 14
    weight 0.211
  ]
  edge
  [
    source 5
    target 15
    weight 0.237
  ]
  edge
  [
    source 5
    target 16
    weight 0.124
  ]
  edge
  [
    source 5
    target 17
    weight 0.006
  ]
  edge
  [
    source 5
    target 18
    weight 0.418
  ]
  edge
  [
    source 5
    target 19
    weight 0.214
  ]
  edge
  [
    source 5
    target 20
    weight 0.513
  ]
  edge
  [
    source 5
    target 21
    weight 0.545
  ]
  edge
  [
    source 5
    target 22
    weight 0.129
  ]
  edge
  [
    source 5
    target 23
    weight 0.242
  ]
  edge
  [
    source 5
    target 24
    weight 0.435
  ]
  edge
  [
    source 5
    target 25
    weight 0.173
  ]
  edge
  [
    source 5
    target 26
    weight 0.004
  ]
  edge
  [
    source 5
    target 27
    weight 0.291
  ]
  edge
  [
    source 5
    target 28
    weight 0.173
  ]
  edge
  [
    source 5
    target 29
    weight 0.109
  ]
  edge
  [
    source 5
    target 30
    weight 0.047
  ]
  edge
  [
    source 5
    target 31
    weight 0.195
  ]
  edge
  [
    source 5
    target 32
    weight 0.435
  ]
  edge
  [
    source 5
    target 33
    weight 0.013
  ]
  edge
  [
    source 5
    target 34
    weight 0
  ]
  edge
  [
    source 5
    target 35
    weight 0.044
  ]
  edge
  [
    source 5
    target 36
    weight 0.13
  ]
  edge
  [
    source 5
    target 37
    weight 0.001
  ]
  edge
  [
    source 5
    target 38
    weight 0.331
  ]
  edge
  [
    source 5
    target 39
    weight 0.025
  ]
  edge
  [
    source 6
    target 0
    weight 0.001
  ]
  edge
  [
    source 6
    target 1
    weight 0.01
  ]
  edge
  [
    source 6
    target 2
    weight 0.012
  ]
  edge
  [
    source 6
    target 3
    weight 0.003
  ]
  edge
  [
    source 6
    target 4
    weight 0.049
  ]
  edge
  [
    source 6
    target 5
    weight 0.14
  ]
  edge
  [
    source 6
    target 6
    weight 0
  ]
  edge
  [
    source 6
    target 7
    weight 0.153
  ]
  edge
  [
    source 6
    target 8
    weight 0.05
  ]
  edge
  [
    source 6
    target 9
    weight 0.03
  ]
  edge
  [
    source 6
    target 10
    weight 0.305
  ]
  edge
  [
    source 6
    target 11
    weight 0.125
  ]
  edge
  [
    source 6
    target 12
    weight 0
  ]
  edge
  [
    source 6
    target 13
    weight 0.003
  ]
  edge
  [
    source 6
    target 14
    weight 0.045
  ]
  edge
  [
    source 6
    target 15
    weight 0.018
  ]
  edge
  [
    source 6
    target 16
    weight 0.017
  ]
  edge
  [
    source 6
    target 17
    weight 0.001
  ]
  edge
  [
    source 6
    target 18
    weight 0.036
  ]
  edge
  [
    source 6
    target 19
    weight 0.014
  ]
  edge
  [
    source 6
    target 20
    weight 0.032
  ]
  edge
  [
    source 6
    target 21
    weight 0.009
  ]
  edge
  [
    source 6
    target 22
    weight 0.003
  ]
  edge
  [
    source 6
    target 23
    weight 0.043
  ]
  edge
  [
    source 6
    target 24
    weight 0.049
  ]
  edge
  [
    source 6
    target 25
    weight 0.009
  ]
  edge
  [
    source 6
    target 26
    weight 0.187
  ]
  edge
  [
    source 6
    target 27
    weight 0.094
  ]
  edge
  [
    source 6
    target 28
    weight 0.049
  ]
  edge
  [
    source 6
    target 29
    weight 0.171
  ]
  edge
  [
    source 6
    target 30
    weight 0.066
  ]
  edge
  [
    source 6
    target 31
    weight 0.002
  ]
  edge
  [
    source 6
    target 32
    weight 0.015
  ]
  edge
  [
    source 6
    target 33
    weight 0.034
  ]
  edge
  [
    source 6
    target 34
    weight 0.068
  ]
  edge
  [
    source 6
    target 35
    weight 0.006
  ]
  edge
  [
    source 6
    target 36
    weight 0
  ]
  edge
  [
    source 6
    target 37
    weight 0.022
  ]
  edge
  [
    source 6
    target 38
    weight 0.039
  ]
  edge
  [
    source 6
    target 39
    weight 0.05
  ]
  edge
  [
    source 7
    target 0
    weight 0.001
  ]
  edge
  [
    source 7
    target 1
    weight 0.019
  ]
  edge
  [
    source 7
    target 2
    weight 0.015
  ]
  edge
  [
    source 7
    target 3
    weight 0.001
  ]
  edge
  [
    source 7
    target 4
    weight 0.21
  ]
  edge
  [
    source 7
    target 5
    weight 0.402
  ]
  edge
  [
    source 7
    target 6
    weight 0.153
  ]
  edge
  [
    source 7
    target 7
    weight 0
  ]
  edge
  [
    source 7
    target 8
    weight 0.255
  ]
  edge
  [
    source 7
    target 9
    weight 0.17
  ]
  edge
  [
    source 7
    target 10
    weight 0.269
  ]
  edge
  [
    source 7
    target 11
    weight 0.071
  ]
  edge
  [
    source 7
    target 12
    weight 0.008
  ]
  edge
  [
    source 7
    target 13
    weight 0
  ]
  edge
  [
    source 7
    target 14
    weight 0.057
  ]
  edge
  [
    source 7
    target 15
    weight 0.071
  ]
  edge
  [
    source 7
    target 16
    weight 0.021
  ]
  edge
  [
    source 7
    target 17
    weight 0.001
  ]
  edge
  [
    source 7
    target 18
    weight 0
  ]
  edge
  [
    source 7
    target 19
    weight 0.001
  ]
  edge
  [
    source 7
    target 20
    weight 0.042
  ]
  edge
  [
    source 7
    target 21
    weight 0.001
  ]
  edge
  [
    source 7
    target 22
    weight 0.002
  ]
  edge
  [
    source 7
    target 23
    weight 0.077
  ]
  edge
  [
    source 7
    target 24
    weight 0.017
  ]
  edge
  [
    source 7
    target 25
    weight 0.094
  ]
  edge
  [
    source 7
    target 26
    weight 0.016
  ]
  edge
  [
    source 7
    target 27
    weight 0.434
  ]
  edge
  [
    source 7
    target 28
    weight 0.513
  ]
  edge
  [
    source 7
    target 29
    weight 0.2
  ]
  edge
  [
    source 7
    target 30
    weight 0.242
  ]
  edge
  [
    source 7
    target 31
    weight 0.005
  ]
  edge
  [
    source 7
    target 32
    weight 0.012
  ]
  edge
  [
    source 7
    target 33
    weight 0.058
  ]
  edge
  [
    source 7
    target 34
    weight 0.079
  ]
  edge
  [
    source 7
    target 35
    weight 0.003
  ]
  edge
  [
    source 7
    target 36
    weight 0.002
  ]
  edge
  [
    source 7
    target 37
    weight 0.004
  ]
  edge
  [
    source 7
    target 38
    weight 0
  ]
  edge
  [
    source 7
    target 39
    weight 0
  ]
  edge
  [
    source 8
    target 0
    weight 0.029
  ]
  edge
  [
    source 8
    target 1
    weight 0.004
  ]
  edge
  [
    source 8
    target 2
    weight 0.108
  ]
  edge
  [
    source 8
    target 3
    weight 0.092
  ]
  edge
  [
    source 8
    target 4
    weight 0.069
  ]
  edge
  [
    source 8
    target 5
    weight 0.109
  ]
  edge
  [
    source 8
    target 6
    weight 0.05
  ]
  edge
  [
    source 8
    target 7
    weight 0.255
  ]
  edge
  [
    source 8
    target 8
    weight 0
  ]
  edge
  [
    source 8
    target 9
    weight 0.33
  ]
  edge
  [
    source 8
    target 10
    weight 0.085
  ]
  edge
  [
    source 8
    target 11
    weight 0.13
  ]
  edge
  [
    source 8
    target 12
    weight 0.058
  ]
  edge
  [
    source 8
    target 13
    weight 0
  ]
  edge
  [
    source 8
    target 14
    weight 0.305
  ]
  edge
  [
    source 8
    target 15
    weight 0.104
  ]
  edge
  [
    source 8
    target 16
    weight 0.073
  ]
  edge
  [
    source 8
    target 17
    weight 0.001
  ]
  edge
  [
    source 8
    target 18
    weight 0.389
  ]
  edge
  [
    source 8
    target 19
    weight 0.359
  ]
  edge
  [
    source 8
    target 20
    weight 0.139
  ]
  edge
  [
    source 8
    target 21
    weight 0.131
  ]
  edge
  [
    source 8
    target 22
    weight 0.058
  ]
  edge
  [
    source 8
    target 23
    weight 0.123
  ]
  edge
  [
    source 8
    target 24
    weight 0.06
  ]
  edge
  [
    source 8
    target 25
    weight 0.031
  ]
  edge
  [
    source 8
    target 26
    weight 0.012
  ]
  edge
  [
    source 8
    target 27
    weight 0.143
  ]
  edge
  [
    source 8
    target 28
    weight 0.186
  ]
  edge
  [
    source 8
    target 29
    weight 0.006
  ]
  edge
  [
    source 8
    target 30
    weight 0
  ]
  edge
  [
    source 8
    target 31
    weight 0.077
  ]
  edge
  [
    source 8
    target 32
    weight 0.205
  ]
  edge
  [
    source 8
    target 33
    weight 0.001
  ]
  edge
  [
    source 8
    target 34
    weight 0.003
  ]
  edge
  [
    source 8
    target 35
    weight 0.116
  ]
  edge
  [
    source 8
    target 36
    weight 0.03
  ]
  edge
  [
    source 8
    target 37
    weight 0.019
  ]
  edge
  [
    source 8
    target 38
    weight 0.303
  ]
  edge
  [
    source 8
    target 39
    weight 0.001
  ]
  edge
  [
    source 9
    target 0
    weight 0
  ]
  edge
  [
    source 9
    target 1
    weight 0.005
  ]
  edge
  [
    source 9
    target 2
    weight 0.295
  ]
  edge
  [
    source 9
    target 3
    weight 0.21
  ]
  edge
  [
    source 9
    target 4
    weight 0.069
  ]
  edge
  [
    source 9
    target 5
    weight 0.255
  ]
  edge
  [
    source 9
    target 6
    weight 0.03
  ]
  edge
  [
    source 9
    target 7
    weight 0.17
  ]
  edge
  [
    source 9
    target 8
    weight 0.33
  ]
  edge
  [
    source 9
    target 9
    weight 0
  ]
  edge
  [
    source 9
    target 10
    weight 0.197
  ]
  edge
  [
    source 9
    target 11
    weight 0.197
  ]
  edge
  [
    source 9
    target 12
    weight 0.161
  ]
  edge
  [
    source 9
    target 13
    weight 0
  ]
  edge
  [
    source 9
    target 14
    weight 0.442
  ]
  edge
  [
    source 9
    target 15
    weight 0.153
  ]
  edge
  [
    source 9
    target 16
    weight 0.141
  ]
  edge
  [
    source 9
    target 17
    weight 0.006
  ]
  edge
  [
    source 9
    target 18
    weight 0.318
  ]
  edge
  [
    source 9
    target 19
    weight 0.384
  ]
  edge
  [
    source 9
    target 20
    weight 0.159
  ]
  edge
  [
    source 9
    target 21
    weight 0.249
  ]
  edge
  [
    source 9
    target 22
    weight 0.084
  ]
  edge
  [
    source 9
    target 23
    weight 0.092
  ]
  edge
  [
    source 9
    target 24
    weight 0.031
  ]
  edge
  [
    source 9
    target 25
    weight 0.029
  ]
  edge
  [
    source 9
    target 26
    weight 0.004
  ]
  edge
  [
    source 9
    target 27
    weight 0.042
  ]
  edge
  [
    source 9
    target 28
    weight 0.077
  ]
  edge
  [
    source 9
    target 29
    weight 0.048
  ]
  edge
  [
    source 9
    target 30
    weight 0.061
  ]
  edge
  [
    source 9
    target 31
    weight 0.138
  ]
  edge
  [
    source 9
    target 32
    weight 0.31
  ]
  edge
  [
    source 9
    target 33
    weight 0
  ]
  edge
  [
    source 9
    target 34
    weight 0.009
  ]
  edge
  [
    source 9
    target 35
    weight 0.067
  ]
  edge
  [
    source 9
    target 36
    weight 0.067
  ]
  edge
  [
    source 9
    target 37
    weight 0.009
  ]
  edge
  [
    source 9
    target 38
    weight 0.214
  ]
  edge
  [
    source 9
    target 39
    weight 0.004
  ]
  edge
  [
    source 10
    target 0
    weight 0.164
  ]
  edge
  [
    source 10
    target 1
    weight 0.028
  ]
  edge
  [
    source 10
    target 2
    weight 0.008
  ]
  edge
  [
    source 10
    target 3
    weight 0.121
  ]
  edge
  [
    source 10
    target 4
    weight 0.332
  ]
  edge
  [
    source 10
    target 5
    weight 0.245
  ]
  edge
  [
    source 10
    target 6
    weight 0.305
  ]
  edge
  [
    source 10
    target 7
    weight 0.269
  ]
  edge
  [
    source 10
    target 8
    weight 0.085
  ]
  edge
  [
    source 10
    target 9
    weight 0.197
  ]
  edge
  [
    source 10
    target 10
    weight 0
  ]
  edge
  [
    source 10
    target 11
    weight 0.893
  ]
  edge
  [
    source 10
    target 12
    weight 0.061
  ]
  edge
  [
    source 10
    target 13
    weight 0.066
  ]
  edge
  [
    source 10
    target 14
    weight 0.076
  ]
  edge
  [
    source 10
    target 15
    weight 0.03
  ]
  edge
  [
    source 10
    target 16
    weight 0.28
  ]
  edge
  [
    source 10
    target 17
    weight 0.275
  ]
  edge
  [
    source 10
    target 18
    weight 0
  ]
  edge
  [
    source 10
    target 19
    weight 0.002
  ]
  edge
  [
    source 10
    target 20
    weight 0.163
  ]
  edge
  [
    source 10
    target 21
    weight 0.149
  ]
  edge
  [
    source 10
    target 22
    weight 0.093
  ]
  edge
  [
    source 10
    target 23
    weight 0.167
  ]
  edge
  [
    source 10
    target 24
    weight 0.135
  ]
  edge
  [
    source 10
    target 25
    weight 0.334
  ]
  edge
  [
    source 10
    target 26
    weight 0.276
  ]
  edge
  [
    source 10
    target 27
    weight 0.21
  ]
  edge
  [
    source 10
    target 28
    weight 0.214
  ]
  edge
  [
    source 10
    target 29
    weight 0.42
  ]
  edge
  [
    source 10
    target 30
    weight 0.208
  ]
  edge
  [
    source 10
    target 31
    weight 0.07
  ]
  edge
  [
    source 10
    target 32
    weight 0.114
  ]
  edge
  [
    source 10
    target 33
    weight 0.248
  ]
  edge
  [
    source 10
    target 34
    weight 0.242
  ]
  edge
  [
    source 10
    target 35
    weight 0.063
  ]
  edge
  [
    source 10
    target 36
    weight 0.044
  ]
  edge
  [
    source 10
    target 37
    weight 0.005
  ]
  edge
  [
    source 10
    target 38
    weight 0.161
  ]
  edge
  [
    source 10
    target 39
    weight 0.022
  ]
  edge
  [
    source 11
    target 0
    weight 0.14
  ]
  edge
  [
    source 11
    target 1
    weight 0.011
  ]
  edge
  [
    source 11
    target 2
    weight 0.004
  ]
  edge
  [
    source 11
    target 3
    weight 0.084
  ]
  edge
  [
    source 11
    target 4
    weight 0.163
  ]
  edge
  [
    source 11
    target 5
    weight 0.251
  ]
  edge
  [
    source 11
    target 6
    weight 0.125
  ]
  edge
  [
    source 11
    target 7
    weight 0.071
  ]
  edge
  [
    source 11
    target 8
    weight 0.13
  ]
  edge
  [
    source 11
    target 9
    weight 0.197
  ]
  edge
  [
    source 11
    target 10
    weight 0.893
  ]
  edge
  [
    source 11
    target 11
    weight 0
  ]
  edge
  [
    source 11
    target 12
    weight 0.056
  ]
  edge
  [
    source 11
    target 13
    weight 0.064
  ]
  edge
  [
    source 11
    target 14
    weight 0.075
  ]
  edge
  [
    source 11
    target 15
    weight 0.049
  ]
  edge
  [
    source 11
    target 16
    weight 0.291
  ]
  edge
  [
    source 11
    target 17
    weight 0.186
  ]
  edge
  [
    source 11
    target 18
    weight 0
  ]
  edge
  [
    source 11
    target 19
    weight 0.001
  ]
  edge
  [
    source 11
    target 20
    weight 0.069
  ]
  edge
  [
    source 11
    target 21
    weight 0.049
  ]
  edge
  [
    source 11
    target 22
    weight 0.093
  ]
  edge
  [
    source 11
    target 23
    weight 0.155
  ]
  edge
  [
    source 11
    target 24
    weight 0.07
  ]
  edge
  [
    source 11
    target 25
    weight 0.218
  ]
  edge
  [
    source 11
    target 26
    weight 0.163
  ]
  edge
  [
    source 11
    target 27
    weight 0.145
  ]
  edge
  [
    source 11
    target 28
    weight 0.054
  ]
  edge
  [
    source 11
    target 29
    weight 0.356
  ]
  edge
  [
    source 11
    target 30
    weight 0.391
  ]
  edge
  [
    source 11
    target 31
    weight 0.025
  ]
  edge
  [
    source 11
    target 32
    weight 0.012
  ]
  edge
  [
    source 11
    target 33
    weight 0.234
  ]
  edge
  [
    source 11
    target 34
    weight 0.16
  ]
  edge
  [
    source 11
    target 35
    weight 0.041
  ]
  edge
  [
    source 11
    target 36
    weight 0.114
  ]
  edge
  [
    source 11
    target 37
    weight 0.009
  ]
  edge
  [
    source 11
    target 38
    weight 0.047
  ]
  edge
  [
    source 11
    target 39
    weight 0.042
  ]
  edge
  [
    source 12
    target 0
    weight 0.025
  ]
  edge
  [
    source 12
    target 1
    weight 0.035
  ]
  edge
  [
    source 12
    target 2
    weight 0.157
  ]
  edge
  [
    source 12
    target 3
    weight 0.211
  ]
  edge
  [
    source 12
    target 4
    weight 0.053
  ]
  edge
  [
    source 12
    target 5
    weight 0.213
  ]
  edge
  [
    source 12
    target 6
    weight 0
  ]
  edge
  [
    source 12
    target 7
    weight 0.008
  ]
  edge
  [
    source 12
    target 8
    weight 0.058
  ]
  edge
  [
    source 12
    target 9
    weight 0.161
  ]
  edge
  [
    source 12
    target 10
    weight 0.061
  ]
  edge
  [
    source 12
    target 11
    weight 0.056
  ]
  edge
  [
    source 12
    target 12
    weight 0
  ]
  edge
  [
    source 12
    target 13
    weight 0.323
  ]
  edge
  [
    source 12
    target 14
    weight 0.103
  ]
  edge
  [
    source 12
    target 15
    weight 0.043
  ]
  edge
  [
    source 12
    target 16
    weight 0.025
  ]
  edge
  [
    source 12
    target 17
    weight 0.084
  ]
  edge
  [
    source 12
    target 18
    weight 0.078
  ]
  edge
  [
    source 12
    target 19
    weight 0.04
  ]
  edge
  [
    source 12
    target 20
    weight 0.001
  ]
  edge
  [
    source 12
    target 21
    weight 0.059
  ]
  edge
  [
    source 12
    target 22
    weight 0
  ]
  edge
  [
    source 12
    target 23
    weight 0.026
  ]
  edge
  [
    source 12
    target 24
    weight 0.001
  ]
  edge
  [
    source 12
    target 25
    weight 0
  ]
  edge
  [
    source 12
    target 26
    weight 0.054
  ]
  edge
  [
    source 12
    target 27
    weight 0.003
  ]
  edge
  [
    source 12
    target 28
    weight 0
  ]
  edge
  [
    source 12
    target 29
    weight 0.004
  ]
  edge
  [
    source 12
    target 30
    weight 0.003
  ]
  edge
  [
    source 12
    target 31
    weight 0.209
  ]
  edge
  [
    source 12
    target 32
    weight 0.023
  ]
  edge
  [
    source 12
    target 33
    weight 0.03
  ]
  edge
  [
    source 12
    target 34
    weight 0
  ]
  edge
  [
    source 12
    target 35
    weight 0.005
  ]
  edge
  [
    source 12
    target 36
    weight 0.003
  ]
  edge
  [
    source 12
    target 37
    weight 0.045
  ]
  edge
  [
    source 12
    target 38
    weight 0.055
  ]
  edge
  [
    source 12
    target 39
    weight 0.003
  ]
  edge
  [
    source 13
    target 0
    weight 0.005
  ]
  edge
  [
    source 13
    target 1
    weight 0.03
  ]
  edge
  [
    source 13
    target 2
    weight 0.188
  ]
  edge
  [
    source 13
    target 3
    weight 0.259
  ]
  edge
  [
    source 13
    target 4
    weight 0.005
  ]
  edge
  [
    source 13
    target 5
    weight 0.064
  ]
  edge
  [
    source 13
    target 6
    weight 0.003
  ]
  edge
  [
    source 13
    target 7
    weight 0
  ]
  edge
  [
    source 13
    target 8
    weight 0
  ]
  edge
  [
    source 13
    target 9
    weight 0
  ]
  edge
  [
    source 13
    target 10
    weight 0.066
  ]
  edge
  [
    source 13
    target 11
    weight 0.064
  ]
  edge
  [
    source 13
    target 12
    weight 0.323
  ]
  edge
  [
    source 13
    target 13
    weight 0
  ]
  edge
  [
    source 13
    target 14
    weight 0.072
  ]
  edge
  [
    source 13
    target 15
    weight 0.01
  ]
  edge
  [
    source 13
    target 16
    weight 0.001
  ]
  edge
  [
    source 13
    target 17
    weight 0.058
  ]
  edge
  [
    source 13
    target 18
    weight 0.097
  ]
  edge
  [
    source 13
    target 19
    weight 0.044
  ]
  edge
  [
    source 13
    target 20
    weight 0.003
  ]
  edge
  [
    source 13
    target 21
    weight 0
  ]
  edge
  [
    source 13
    target 22
    weight 0.012
  ]
  edge
  [
    source 13
    target 23
    weight 0.022
  ]
  edge
  [
    source 13
    target 24
    weight 0.001
  ]
  edge
  [
    source 13
    target 25
    weight 0.134
  ]
  edge
  [
    source 13
    target 26
    weight 0.092
  ]
  edge
  [
    source 13
    target 27
    weight 0.003
  ]
  edge
  [
    source 13
    target 28
    weight 0.003
  ]
  edge
  [
    source 13
    target 29
    weight 0.009
  ]
  edge
  [
    source 13
    target 30
    weight 0.025
  ]
  edge
  [
    source 13
    target 31
    weight 0.126
  ]
  edge
  [
    source 13
    target 32
    weight 0.036
  ]
  edge
  [
    source 13
    target 33
    weight 0.045
  ]
  edge
  [
    source 13
    target 34
    weight 0
  ]
  edge
  [
    source 13
    target 35
    weight 0.001
  ]
  edge
  [
    source 13
    target 36
    weight 0.018
  ]
  edge
  [
    source 13
    target 37
    weight 0.056
  ]
  edge
  [
    source 13
    target 38
    weight 0.114
  ]
  edge
  [
    source 13
    target 39
    weight 0.035
  ]
  edge
  [
    source 14
    target 0
    weight 0.001
  ]
  edge
  [
    source 14
    target 1
    weight 0.164
  ]
  edge
  [
    source 14
    target 2
    weight 0
  ]
  edge
  [
    source 14
    target 3
    weight 0.011
  ]
  edge
  [
    source 14
    target 4
    weight 0.082
  ]
  edge
  [
    source 14
    target 5
    weight 0.211
  ]
  edge
  [
    source 14
    target 6
    weight 0.045
  ]
  edge
  [
    source 14
    target 7
    weight 0.057
  ]
  edge
  [
    source 14
    target 8
    weight 0.305
  ]
  edge
  [
    source 14
    target 9
    weight 0.442
  ]
  edge
  [
    source 14
    target 10
    weight 0.076
  ]
  edge
  [
    source 14
    target 11
    weight 0.075
  ]
  edge
  [
    source 14
    target 12
    weight 0.103
  ]
  edge
  [
    source 14
    target 13
    weight 0.072
  ]
  edge
  [
    source 14
    target 14
    weight 0
  ]
  edge
  [
    source 14
    target 15
    weight 0.005
  ]
  edge
  [
    source 14
    target 16
    weight 0.156
  ]
  edge
  [
    source 14
    target 17
    weight 0.214
  ]
  edge
  [
    source 14
    target 18
    weight 0.173
  ]
  edge
  [
    source 14
    target 19
    weight 0.053
  ]
  edge
  [
    source 14
    target 20
    weight 0.28
  ]
  edge
  [
    source 14
    target 21
    weight 0.277
  ]
  edge
  [
    source 14
    target 22
    weight 0.107
  ]
  edge
  [
    source 14
    target 23
    weight 0.42
  ]
  edge
  [
    source 14
    target 24
    weight 0.441
  ]
  edge
  [
    source 14
    target 25
    weight 0.094
  ]
  edge
  [
    source 14
    target 26
    weight 0.163
  ]
  edge
  [
    source 14
    target 27
    weight 0.13
  ]
  edge
  [
    source 14
    target 28
    weight 0.23
  ]
  edge
  [
    source 14
    target 29
    weight 0.064
  ]
  edge
  [
    source 14
    target 30
    weight 0.01
  ]
  edge
  [
    source 14
    target 31
    weight 0
  ]
  edge
  [
    source 14
    target 32
    weight 0.194
  ]
  edge
  [
    source 14
    target 33
    weight 0.03
  ]
  edge
  [
    source 14
    target 34
    weight 0.022
  ]
  edge
  [
    source 14
    target 35
    weight 0
  ]
  edge
  [
    source 14
    target 36
    weight 0.046
  ]
  edge
  [
    source 14
    target 37
    weight 0.001
  ]
  edge
  [
    source 14
    target 38
    weight 0.48
  ]
  edge
  [
    source 14
    target 39
    weight 0.058
  ]
  edge
  [
    source 15
    target 0
    weight 0
  ]
  edge
  [
    source 15
    target 1
    weight 0.069
  ]
  edge
  [
    source 15
    target 2
    weight 0.008
  ]
  edge
  [
    source 15
    target 3
    weight 0.001
  ]
  edge
  [
    source 15
    target 4
    weight 0.129
  ]
  edge
  [
    source 15
    target 5
    weight 0.237
  ]
  edge
  [
    source 15
    target 6
    weight 0.018
  ]
  edge
  [
    source 15
    target 7
    weight 0.071
  ]
  edge
  [
    source 15
    target 8
    weight 0.104
  ]
  edge
  [
    source 15
    target 9
    weight 0.153
  ]
  edge
  [
    source 15
    target 10
    weight 0.03
  ]
  edge
  [
    source 15
    target 11
    weight 0.049
  ]
  edge
  [
    source 15
    target 12
    weight 0.043
  ]
  edge
  [
    source 15
    target 13
    weight 0.01
  ]
  edge
  [
    source 15
    target 14
    weight 0.005
  ]
  edge
  [
    source 15
    target 15
    weight 0
  ]
  edge
  [
    source 15
    target 16
    weight 0.089
  ]
  edge
  [
    source 15
    target 17
    weight 0.148
  ]
  edge
  [
    source 15
    target 18
    weight 0.05
  ]
  edge
  [
    source 15
    target 19
    weight 0.006
  ]
  edge
  [
    source 15
    target 20
    weight 0.194
  ]
  edge
  [
    source 15
    target 21
    weight 0.078
  ]
  edge
  [
    source 15
    target 22
    weight 0.095
  ]
  edge
  [
    source 15
    target 23
    weight 0.082
  ]
  edge
  [
    source 15
    target 24
    weight 0.339
  ]
  edge
  [
    source 15
    target 25
    weight 0.037
  ]
  edge
  [
    source 15
    target 26
    weight 0.073
  ]
  edge
  [
    source 15
    target 27
    weight 0.015
  ]
  edge
  [
    source 15
    target 28
    weight 0.061
  ]
  edge
  [
    source 15
    target 29
    weight 0.018
  ]
  edge
  [
    source 15
    target 30
    weight 0.006
  ]
  edge
  [
    source 15
    target 31
    weight 0.003
  ]
  edge
  [
    source 15
    target 32
    weight 0.154
  ]
  edge
  [
    source 15
    target 33
    weight 0.041
  ]
  edge
  [
    source 15
    target 34
    weight 0.039
  ]
  edge
  [
    source 15
    target 35
    weight 0.068
  ]
  edge
  [
    source 15
    target 36
    weight 0.126
  ]
  edge
  [
    source 15
    target 37
    weight 0
  ]
  edge
  [
    source 15
    target 38
    weight 0.278
  ]
  edge
  [
    source 15
    target 39
    weight 0.01
  ]
  edge
  [
    source 16
    target 0
    weight 0.06
  ]
  edge
  [
    source 16
    target 1
    weight 0.144
  ]
  edge
  [
    source 16
    target 2
    weight 0.579
  ]
  edge
  [
    source 16
    target 3
    weight 0.455
  ]
  edge
  [
    source 16
    target 4
    weight 0.025
  ]
  edge
  [
    source 16
    target 5
    weight 0.124
  ]
  edge
  [
    source 16
    target 6
    weight 0.017
  ]
  edge
  [
    source 16
    target 7
    weight 0.021
  ]
  edge
  [
    source 16
    target 8
    weight 0.073
  ]
  edge
  [
    source 16
    target 9
    weight 0.141
  ]
  edge
  [
    source 16
    target 10
    weight 0.28
  ]
  edge
  [
    source 16
    target 11
    weight 0.291
  ]
  edge
  [
    source 16
    target 12
    weight 0.025
  ]
  edge
  [
    source 16
    target 13
    weight 0.001
  ]
  edge
  [
    source 16
    target 14
    weight 0.156
  ]
  edge
  [
    source 16
    target 15
    weight 0.089
  ]
  edge
  [
    source 16
    target 16
    weight 0
  ]
  edge
  [
    source 16
    target 17
    weight 0.437
  ]
  edge
  [
    source 16
    target 18
    weight 0.518
  ]
  edge
  [
    source 16
    target 19
    weight 0.575
  ]
  edge
  [
    source 16
    target 20
    weight 0.081
  ]
  edge
  [
    source 16
    target 21
    weight 0.163
  ]
  edge
  [
    source 16
    target 22
    weight 0.035
  ]
  edge
  [
    source 16
    target 23
    weight 0.036
  ]
  edge
  [
    source 16
    target 24
    weight 0.445
  ]
  edge
  [
    source 16
    target 25
    weight 0.001
  ]
  edge
  [
    source 16
    target 26
    weight 0.007
  ]
  edge
  [
    source 16
    target 27
    weight 0.152
  ]
  edge
  [
    source 16
    target 28
    weight 0.173
  ]
  edge
  [
    source 16
    target 29
    weight 0.272
  ]
  edge
  [
    source 16
    target 30
    weight 0.428
  ]
  edge
  [
    source 16
    target 31
    weight 0.243
  ]
  edge
  [
    source 16
    target 32
    weight 0.139
  ]
  edge
  [
    source 16
    target 33
    weight 0.008
  ]
  edge
  [
    source 16
    target 34
    weight 0
  ]
  edge
  [
    source 16
    target 35
    weight 0.015
  ]
  edge
  [
    source 16
    target 36
    weight 0.177
  ]
  edge
  [
    source 16
    target 37
    weight 0.017
  ]
  edge
  [
    source 16
    target 38
    weight 0.288
  ]
  edge
  [
    source 16
    target 39
    weight 0.017
  ]
  edge
  [
    source 17
    target 0
    weight 0.151
  ]
  edge
  [
    source 17
    target 1
    weight 0.071
  ]
  edge
  [
    source 17
    target 2
    weight 0.55
  ]
  edge
  [
    source 17
    target 3
    weight 0.462
  ]
  edge
  [
    source 17
    target 4
    weight 0
  ]
  edge
  [
    source 17
    target 5
    weight 0.006
  ]
  edge
  [
    source 17
    target 6
    weight 0.001
  ]
  edge
  [
    source 17
    target 7
    weight 0.001
  ]
  edge
  [
    source 17
    target 8
    weight 0.001
  ]
  edge
  [
    source 17
    target 9
    weight 0.006
  ]
  edge
  [
    source 17
    target 10
    weight 0.275
  ]
  edge
  [
    source 17
    target 11
    weight 0.186
  ]
  edge
  [
    source 17
    target 12
    weight 0.084
  ]
  edge
  [
    source 17
    target 13
    weight 0.058
  ]
  edge
  [
    source 17
    target 14
    weight 0.214
  ]
  edge
  [
    source 17
    target 15
    weight 0.148
  ]
  edge
  [
    source 17
    target 16
    weight 0.437
  ]
  edge
  [
    source 17
    target 17
    weight 0
  ]
  edge
  [
    source 17
    target 18
    weight 0.238
  ]
  edge
  [
    source 17
    target 19
    weight 0.266
  ]
  edge
  [
    source 17
    target 20
    weight 0.01
  ]
  edge
  [
    source 17
    target 21
    weight 0.037
  ]
  edge
  [
    source 17
    target 22
    weight 0.001
  ]
  edge
  [
    source 17
    target 23
    weight 0.004
  ]
  edge
  [
    source 17
    target 24
    weight 0.247
  ]
  edge
  [
    source 17
    target 25
    weight 0.008
  ]
  edge
  [
    source 17
    target 26
    weight 0.016
  ]
  edge
  [
    source 17
    target 27
    weight 0.016
  ]
  edge
  [
    source 17
    target 28
    weight 0.031
  ]
  edge
  [
    source 17
    target 29
    weight 0.209
  ]
  edge
  [
    source 17
    target 30
    weight 0.238
  ]
  edge
  [
    source 17
    target 31
    weight 0.089
  ]
  edge
  [
    source 17
    target 32
    weight 0.126
  ]
  edge
  [
    source 17
    target 33
    weight 0.018
  ]
  edge
  [
    source 17
    target 34
    weight 0.015
  ]
  edge
  [
    source 17
    target 35
    weight 0.013
  ]
  edge
  [
    source 17
    target 36
    weight 0.091
  ]
  edge
  [
    source 17
    target 37
    weight 0
  ]
  edge
  [
    source 17
    target 38
    weight 0.033
  ]
  edge
  [
    source 17
    target 39
    weight 0.036
  ]
  edge
  [
    source 18
    target 0
    weight 0.074
  ]
  edge
  [
    source 18
    target 1
    weight 0.034
  ]
  edge
  [
    source 18
    target 2
    weight 0.006
  ]
  edge
  [
    source 18
    target 3
    weight 0.058
  ]
  edge
  [
    source 18
    target 4
    weight 0.463
  ]
  edge
  [
    source 18
    target 5
    weight 0.418
  ]
  edge
  [
    source 18
    target 6
    weight 0.036
  ]
  edge
  [
    source 18
    target 7
    weight 0
  ]
  edge
  [
    source 18
    target 8
    weight 0.389
  ]
  edge
  [
    source 18
    target 9
    weight 0.318
  ]
  edge
  [
    source 18
    target 10
    weight 0
  ]
  edge
  [
    source 18
    target 11
    weight 0
  ]
  edge
  [
    source 18
    target 12
    weight 0.078
  ]
  edge
  [
    source 18
    target 13
    weight 0.097
  ]
  edge
  [
    source 18
    target 14
    weight 0.173
  ]
  edge
  [
    source 18
    target 15
    weight 0.05
  ]
  edge
  [
    source 18
    target 16
    weight 0.518
  ]
  edge
  [
    source 18
    target 17
    weight 0.238
  ]
  edge
  [
    source 18
    target 18
    weight 0
  ]
  edge
  [
    source 18
    target 19
    weight 0.837
  ]
  edge
  [
    source 18
    target 20
    weight 0.156
  ]
  edge
  [
    source 18
    target 21
    weight 0.201
  ]
  edge
  [
    source 18
    target 22
    weight 0.067
  ]
  edge
  [
    source 18
    target 23
    weight 0.118
  ]
  edge
  [
    source 18
    target 24
    weight 0.222
  ]
  edge
  [
    source 18
    target 25
    weight 0.165
  ]
  edge
  [
    source 18
    target 26
    weight 0.193
  ]
  edge
  [
    source 18
    target 27
    weight 0.038
  ]
  edge
  [
    source 18
    target 28
    weight 0.038
  ]
  edge
  [
    source 18
    target 29
    weight 0.117
  ]
  edge
  [
    source 18
    target 30
    weight 0.03
  ]
  edge
  [
    source 18
    target 31
    weight 0.022
  ]
  edge
  [
    source 18
    target 32
    weight 0.042
  ]
  edge
  [
    source 18
    target 33
    weight 0.024
  ]
  edge
  [
    source 18
    target 34
    weight 0.037
  ]
  edge
  [
    source 18
    target 35
    weight 0.003
  ]
  edge
  [
    source 18
    target 36
    weight 0.012
  ]
  edge
  [
    source 18
    target 37
    weight 0.008
  ]
  edge
  [
    source 18
    target 38
    weight 0.249
  ]
  edge
  [
    source 18
    target 39
    weight 0.015
  ]
  edge
  [
    source 19
    target 0
    weight 0.572
  ]
  edge
  [
    source 19
    target 1
    weight 0.003
  ]
  edge
  [
    source 19
    target 2
    weight 0.009
  ]
  edge
  [
    source 19
    target 3
    weight 0.047
  ]
  edge
  [
    source 19
    target 4
    weight 0.566
  ]
  edge
  [
    source 19
    target 5
    weight 0.214
  ]
  edge
  [
    source 19
    target 6
    weight 0.014
  ]
  edge
  [
    source 19
    target 7
    weight 0.001
  ]
  edge
  [
    source 19
    target 8
    weight 0.359
  ]
  edge
  [
    source 19
    target 9
    weight 0.384
  ]
  edge
  [
    source 19
    target 10
    weight 0.002
  ]
  edge
  [
    source 19
    target 11
    weight 0.001
  ]
  edge
  [
    source 19
    target 12
    weight 0.04
  ]
  edge
  [
    source 19
    target 13
    weight 0.044
  ]
  edge
  [
    source 19
    target 14
    weight 0.053
  ]
  edge
  [
    source 19
    target 15
    weight 0.006
  ]
  edge
  [
    source 19
    target 16
    weight 0.575
  ]
  edge
  [
    source 19
    target 17
    weight 0.266
  ]
  edge
  [
    source 19
    target 18
    weight 0.837
  ]
  edge
  [
    source 19
    target 19
    weight 0
  ]
  edge
  [
    source 19
    target 20
    weight 0.086
  ]
  edge
  [
    source 19
    target 21
    weight 0.061
  ]
  edge
  [
    source 19
    target 22
    weight 0.031
  ]
  edge
  [
    source 19
    target 23
    weight 0.01
  ]
  edge
  [
    source 19
    target 24
    weight 0.123
  ]
  edge
  [
    source 19
    target 25
    weight 0.144
  ]
  edge
  [
    source 19
    target 26
    weight 0.099
  ]
  edge
  [
    source 19
    target 27
    weight 0.036
  ]
  edge
  [
    source 19
    target 28
    weight 0
  ]
  edge
  [
    source 19
    target 29
    weight 0.074
  ]
  edge
  [
    source 19
    target 30
    weight 0.115
  ]
  edge
  [
    source 19
    target 31
    weight 0.021
  ]
  edge
  [
    source 19
    target 32
    weight 0.005
  ]
  edge
  [
    source 19
    target 33
    weight 0.068
  ]
  edge
  [
    source 19
    target 34
    weight 0.1
  ]
  edge
  [
    source 19
    target 35
    weight 0.001
  ]
  edge
  [
    source 19
    target 36
    weight 0.005
  ]
  edge
  [
    source 19
    target 37
    weight 0.013
  ]
  edge
  [
    source 19
    target 38
    weight 0.086
  ]
  edge
  [
    source 19
    target 39
    weight 0.026
  ]
  edge
  [
    source 20
    target 0
    weight 0.002
  ]
  edge
  [
    source 20
    target 1
    weight 0.063
  ]
  edge
  [
    source 20
    target 2
    weight 0.113
  ]
  edge
  [
    source 20
    target 3
    weight 0.134
  ]
  edge
  [
    source 20
    target 4
    weight 0.213
  ]
  edge
  [
    source 20
    target 5
    weight 0.513
  ]
  edge
  [
    source 20
    target 6
    weight 0.032
  ]
  edge
  [
    source 20
    target 7
    weight 0.042
  ]
  edge
  [
    source 20
    target 8
    weight 0.139
  ]
  edge
  [
    source 20
    target 9
    weight 0.159
  ]
  edge
  [
    source 20
    target 10
    weight 0.163
  ]
  edge
  [
    source 20
    target 11
    weight 0.069
  ]
  edge
  [
    source 20
    target 12
    weight 0.001
  ]
  edge
  [
    source 20
    target 13
    weight 0.003
  ]
  edge
  [
    source 20
    target 14
    weight 0.28
  ]
  edge
  [
    source 20
    target 15
    weight 0.194
  ]
  edge
  [
    source 20
    target 16
    weight 0.081
  ]
  edge
  [
    source 20
    target 17
    weight 0.01
  ]
  edge
  [
    source 20
    target 18
    weight 0.156
  ]
  edge
  [
    source 20
    target 19
    weight 0.086
  ]
  edge
  [
    source 20
    target 20
    weight 0
  ]
  edge
  [
    source 20
    target 21
    weight 0.178
  ]
  edge
  [
    source 20
    target 22
    weight 0.086
  ]
  edge
  [
    source 20
    target 23
    weight 0.08
  ]
  edge
  [
    source 20
    target 24
    weight 0.194
  ]
  edge
  [
    source 20
    target 25
    weight 0.001
  ]
  edge
  [
    source 20
    target 26
    weight 0.188
  ]
  edge
  [
    source 20
    target 27
    weight 0.139
  ]
  edge
  [
    source 20
    target 28
    weight 0.144
  ]
  edge
  [
    source 20
    target 29
    weight 0.132
  ]
  edge
  [
    source 20
    target 30
    weight 0.133
  ]
  edge
  [
    source 20
    target 31
    weight 0.105
  ]
  edge
  [
    source 20
    target 32
    weight 0.14
  ]
  edge
  [
    source 20
    target 33
    weight 0.01
  ]
  edge
  [
    source 20
    target 34
    weight 0.006
  ]
  edge
  [
    source 20
    target 35
    weight 0.003
  ]
  edge
  [
    source 20
    target 36
    weight 0
  ]
  edge
  [
    source 20
    target 37
    weight 0.036
  ]
  edge
  [
    source 20
    target 38
    weight 0.205
  ]
  edge
  [
    source 20
    target 39
    weight 0.032
  ]
  edge
  [
    source 21
    target 0
    weight 0.051
  ]
  edge
  [
    source 21
    target 1
    weight 0.026
  ]
  edge
  [
    source 21
    target 2
    weight 0.076
  ]
  edge
  [
    source 21
    target 3
    weight 0.069
  ]
  edge
  [
    source 21
    target 4
    weight 0.328
  ]
  edge
  [
    source 21
    target 5
    weight 0.545
  ]
  edge
  [
    source 21
    target 6
    weight 0.009
  ]
  edge
  [
    source 21
    target 7
    weight 0.001
  ]
  edge
  [
    source 21
    target 8
    weight 0.131
  ]
  edge
  [
    source 21
    target 9
    weight 0.249
  ]
  edge
  [
    source 21
    target 10
    weight 0.149
  ]
  edge
  [
    source 21
    target 11
    weight 0.049
  ]
  edge
  [
    source 21
    target 12
    weight 0.059
  ]
  edge
  [
    source 21
    target 13
    weight 0
  ]
  edge
  [
    source 21
    target 14
    weight 0.277
  ]
  edge
  [
    source 21
    target 15
    weight 0.078
  ]
  edge
  [
    source 21
    target 16
    weight 0.163
  ]
  edge
  [
    source 21
    target 17
    weight 0.037
  ]
  edge
  [
    source 21
    target 18
    weight 0.201
  ]
  edge
  [
    source 21
    target 19
    weight 0.061
  ]
  edge
  [
    source 21
    target 20
    weight 0.178
  ]
  edge
  [
    source 21
    target 21
    weight 0
  ]
  edge
  [
    source 21
    target 22
    weight 0.173
  ]
  edge
  [
    source 21
    target 23
    weight 0.165
  ]
  edge
  [
    source 21
    target 24
    weight 0.137
  ]
  edge
  [
    source 21
    target 25
    weight 0.06
  ]
  edge
  [
    source 21
    target 26
    weight 0.313
  ]
  edge
  [
    source 21
    target 27
    weight 0.072
  ]
  edge
  [
    source 21
    target 28
    weight 0.015
  ]
  edge
  [
    source 21
    target 29
    weight 0.052
  ]
  edge
  [
    source 21
    target 30
    weight 0.043
  ]
  edge
  [
    source 21
    target 31
    weight 0.138
  ]
  edge
  [
    source 21
    target 32
    weight 0.203
  ]
  edge
  [
    source 21
    target 33
    weight 0.034
  ]
  edge
  [
    source 21
    target 34
    weight 0.025
  ]
  edge
  [
    source 21
    target 35
    weight 0.156
  ]
  edge
  [
    source 21
    target 36
    weight 0.082
  ]
  edge
  [
    source 21
    target 37
    weight 0.015
  ]
  edge
  [
    source 21
    target 38
    weight 0.199
  ]
  edge
  [
    source 21
    target 39
    weight 0.015
  ]
  edge
  [
    source 22
    target 0
    weight 0.01
  ]
  edge
  [
    source 22
    target 1
    weight 0.033
  ]
  edge
  [
    source 22
    target 2
    weight 0.056
  ]
  edge
  [
    source 22
    target 3
    weight 0.084
  ]
  edge
  [
    source 22
    target 4
    weight 0.016
  ]
  edge
  [
    source 22
    target 5
    weight 0.129
  ]
  edge
  [
    source 22
    target 6
    weight 0.003
  ]
  edge
  [
    source 22
    target 7
    weight 0.002
  ]
  edge
  [
    source 22
    target 8
    weight 0.058
  ]
  edge
  [
    source 22
    target 9
    weight 0.084
  ]
  edge
  [
    source 22
    target 10
    weight 0.093
  ]
  edge
  [
    source 22
    target 11
    weight 0.093
  ]
  edge
  [
    source 22
    target 12
    weight 0
  ]
  edge
  [
    source 22
    target 13
    weight 0.012
  ]
  edge
  [
    source 22
    target 14
    weight 0.107
  ]
  edge
  [
    source 22
    target 15
    weight 0.095
  ]
  edge
  [
    source 22
    target 16
    weight 0.035
  ]
  edge
  [
    source 22
    target 17
    weight 0.001
  ]
  edge
  [
    source 22
    target 18
    weight 0.067
  ]
  edge
  [
    source 22
    target 19
    weight 0.031
  ]
  edge
  [
    source 22
    target 20
    weight 0.086
  ]
  edge
  [
    source 22
    target 21
    weight 0.173
  ]
  edge
  [
    source 22
    target 22
    weight 0
  ]
  edge
  [
    source 22
    target 23
    weight 0.006
  ]
  edge
  [
    source 22
    target 24
    weight 0
  ]
  edge
  [
    source 22
    target 25
    weight 0.003
  ]
  edge
  [
    source 22
    target 26
    weight 0.069
  ]
  edge
  [
    source 22
    target 27
    weight 0.043
  ]
  edge
  [
    source 22
    target 28
    weight 0.001
  ]
  edge
  [
    source 22
    target 29
    weight 0.104
  ]
  edge
  [
    source 22
    target 30
    weight 0.032
  ]
  edge
  [
    source 22
    target 31
    weight 0.018
  ]
  edge
  [
    source 22
    target 32
    weight 0.11
  ]
  edge
  [
    source 22
    target 33
    weight 0.011
  ]
  edge
  [
    source 22
    target 34
    weight 0.143
  ]
  edge
  [
    source 22
    target 35
    weight 0.005
  ]
  edge
  [
    source 22
    target 36
    weight 0
  ]
  edge
  [
    source 22
    target 37
    weight 0.001
  ]
  edge
  [
    source 22
    target 38
    weight 0.161
  ]
  edge
  [
    source 22
    target 39
    weight 0.005
  ]
  edge
  [
    source 23
    target 0
    weight 0.288
  ]
  edge
  [
    source 23
    target 1
    weight 0.138
  ]
  edge
  [
    source 23
    target 2
    weight 0.121
  ]
  edge
  [
    source 23
    target 3
    weight 0.082
  ]
  edge
  [
    source 23
    target 4
    weight 0.028
  ]
  edge
  [
    source 23
    target 5
    weight 0.242
  ]
  edge
  [
    source 23
    target 6
    weight 0.043
  ]
  edge
  [
    source 23
    target 7
    weight 0.077
  ]
  edge
  [
    source 23
    target 8
    weight 0.123
  ]
  edge
  [
    source 23
    target 9
    weight 0.092
  ]
  edge
  [
    source 23
    target 10
    weight 0.167
  ]
  edge
  [
    source 23
    target 11
    weight 0.155
  ]
  edge
  [
    source 23
    target 12
    weight 0.026
  ]
  edge
  [
    source 23
    target 13
    weight 0.022
  ]
  edge
  [
    source 23
    target 14
    weight 0.42
  ]
  edge
  [
    source 23
    target 15
    weight 0.082
  ]
  edge
  [
    source 23
    target 16
    weight 0.036
  ]
  edge
  [
    source 23
    target 17
    weight 0.004
  ]
  edge
  [
    source 23
    target 18
    weight 0.118
  ]
  edge
  [
    source 23
    target 19
    weight 0.01
  ]
  edge
  [
    source 23
    target 20
    weight 0.08
  ]
  edge
  [
    source 23
    target 21
    weight 0.165
  ]
  edge
  [
    source 23
    target 22
    weight 0.006
  ]
  edge
  [
    source 23
    target 23
    weight 0
  ]
  edge
  [
    source 23
    target 24
    weight 0.011
  ]
  edge
  [
    source 23
    target 25
    weight 0.06
  ]
  edge
  [
    source 23
    target 26
    weight 0.066
  ]
  edge
  [
    source 23
    target 27
    weight 0.001
  ]
  edge
  [
    source 23
    target 28
    weight 0.041
  ]
  edge
  [
    source 23
    target 29
    weight 0.198
  ]
  edge
  [
    source 23
    target 30
    weight 0.162
  ]
  edge
  [
    source 23
    target 31
    weight 0.132
  ]
  edge
  [
    source 23
    target 32
    weight 0.11
  ]
  edge
  [
    source 23
    target 33
    weight 0.164
  ]
  edge
  [
    source 23
    target 34
    weight 0.255
  ]
  edge
  [
    source 23
    target 35
    weight 0.001
  ]
  edge
  [
    source 23
    target 36
    weight 0
  ]
  edge
  [
    source 23
    target 37
    weight 0.012
  ]
  edge
  [
    source 23
    target 38
    weight 0.189
  ]
  edge
  [
    source 23
    target 39
    weight 0.037
  ]
  edge
  [
    source 24
    target 0
    weight 0.312
  ]
  edge
  [
    source 24
    target 1
    weight 0.2
  ]
  edge
  [
    source 24
    target 2
    weight 0.32
  ]
  edge
  [
    source 24
    target 3
    weight 0.057
  ]
  edge
  [
    source 24
    target 4
    weight 0.284
  ]
  edge
  [
    source 24
    target 5
    weight 0.435
  ]
  edge
  [
    source 24
    target 6
    weight 0.049
  ]
  edge
  [
    source 24
    target 7
    weight 0.017
  ]
  edge
  [
    source 24
    target 8
    weight 0.06
  ]
  edge
  [
    source 24
    target 9
    weight 0.031
  ]
  edge
  [
    source 24
    target 10
    weight 0.135
  ]
  edge
  [
    source 24
    target 11
    weight 0.07
  ]
  edge
  [
    source 24
    target 12
    weight 0.001
  ]
  edge
  [
    source 24
    target 13
    weight 0.001
  ]
  edge
  [
    source 24
    target 14
    weight 0.441
  ]
  edge
  [
    source 24
    target 15
    weight 0.339
  ]
  edge
  [
    source 24
    target 16
    weight 0.445
  ]
  edge
  [
    source 24
    target 17
    weight 0.247
  ]
  edge
  [
    source 24
    target 18
    weight 0.222
  ]
  edge
  [
    source 24
    target 19
    weight 0.123
  ]
  edge
  [
    source 24
    target 20
    weight 0.194
  ]
  edge
  [
    source 24
    target 21
    weight 0.137
  ]
  edge
  [
    source 24
    target 22
    weight 0
  ]
  edge
  [
    source 24
    target 23
    weight 0.011
  ]
  edge
  [
    source 24
    target 24
    weight 0
  ]
  edge
  [
    source 24
    target 25
    weight 0.036
  ]
  edge
  [
    source 24
    target 26
    weight 0.001
  ]
  edge
  [
    source 24
    target 27
    weight 0.047
  ]
  edge
  [
    source 24
    target 28
    weight 0.007
  ]
  edge
  [
    source 24
    target 29
    weight 0.072
  ]
  edge
  [
    source 24
    target 30
    weight 0.103
  ]
  edge
  [
    source 24
    target 31
    weight 0.031
  ]
  edge
  [
    source 24
    target 32
    weight 0.035
  ]
  edge
  [
    source 24
    target 33
    weight 0.088
  ]
  edge
  [
    source 24
    target 34
    weight 0.054
  ]
  edge
  [
    source 24
    target 35
    weight 0.052
  ]
  edge
  [
    source 24
    target 36
    weight 0.01
  ]
  edge
  [
    source 24
    target 37
    weight 0.036
  ]
  edge
  [
    source 24
    target 38
    weight 0.322
  ]
  edge
  [
    source 24
    target 39
    weight 0.002
  ]
  edge
  [
    source 25
    target 0
    weight 0.002
  ]
  edge
  [
    source 25
    target 1
    weight 0.091
  ]
  edge
  [
    source 25
    target 2
    weight 0.308
  ]
  edge
  [
    source 25
    target 3
    weight 0.379
  ]
  edge
  [
    source 25
    target 4
    weight 0.031
  ]
  edge
  [
    source 25
    target 5
    weight 0.173
  ]
  edge
  [
    source 25
    target 6
    weight 0.009
  ]
  edge
  [
    source 25
    target 7
    weight 0.094
  ]
  edge
  [
    source 25
    target 8
    weight 0.031
  ]
  edge
  [
    source 25
    target 9
    weight 0.029
  ]
  edge
  [
    source 25
    target 10
    weight 0.334
  ]
  edge
  [
    source 25
    target 11
    weight 0.218
  ]
  edge
  [
    source 25
    target 12
    weight 0
  ]
  edge
  [
    source 25
    target 13
    weight 0.134
  ]
  edge
  [
    source 25
    target 14
    weight 0.094
  ]
  edge
  [
    source 25
    target 15
    weight 0.037
  ]
  edge
  [
    source 25
    target 16
    weight 0.001
  ]
  edge
  [
    source 25
    target 17
    weight 0.008
  ]
  edge
  [
    source 25
    target 18
    weight 0.165
  ]
  edge
  [
    source 25
    target 19
    weight 0.144
  ]
  edge
  [
    source 25
    target 20
    weight 0.001
  ]
  edge
  [
    source 25
    target 21
    weight 0.06
  ]
  edge
  [
    source 25
    target 22
    weight 0.003
  ]
  edge
  [
    source 25
    target 23
    weight 0.06
  ]
  edge
  [
    source 25
    target 24
    weight 0.036
  ]
  edge
  [
    source 25
    target 25
    weight 0
  ]
  edge
  [
    source 25
    target 26
    weight 0.022
  ]
  edge
  [
    source 25
    target 27
    weight 0.019
  ]
  edge
  [
    source 25
    target 28
    weight 0.038
  ]
  edge
  [
    source 25
    target 29
    weight 0.334
  ]
  edge
  [
    source 25
    target 30
    weight 0.323
  ]
  edge
  [
    source 25
    target 31
    weight 0.128
  ]
  edge
  [
    source 25
    target 32
    weight 0.062
  ]
  edge
  [
    source 25
    target 33
    weight 0.104
  ]
  edge
  [
    source 25
    target 34
    weight 0.109
  ]
  edge
  [
    source 25
    target 35
    weight 0.016
  ]
  edge
  [
    source 25
    target 36
    weight 0.009
  ]
  edge
  [
    source 25
    target 37
    weight 0.01
  ]
  edge
  [
    source 25
    target 38
    weight 0.116
  ]
  edge
  [
    source 25
    target 39
    weight 0.033
  ]
  edge
  [
    source 26
    target 0
    weight 0.002
  ]
  edge
  [
    source 26
    target 1
    weight 0.042
  ]
  edge
  [
    source 26
    target 2
    weight 0.564
  ]
  edge
  [
    source 26
    target 3
    weight 0.463
  ]
  edge
  [
    source 26
    target 4
    weight 0.058
  ]
  edge
  [
    source 26
    target 5
    weight 0.004
  ]
  edge
  [
    source 26
    target 6
    weight 0.187
  ]
  edge
  [
    source 26
    target 7
    weight 0.016
  ]
  edge
  [
    source 26
    target 8
    weight 0.012
  ]
  edge
  [
    source 26
    target 9
    weight 0.004
  ]
  edge
  [
    source 26
    target 10
    weight 0.276
  ]
  edge
  [
    source 26
    target 11
    weight 0.163
  ]
  edge
  [
    source 26
    target 12
    weight 0.054
  ]
  edge
  [
    source 26
    target 13
    weight 0.092
  ]
  edge
  [
    source 26
    target 14
    weight 0.163
  ]
  edge
  [
    source 26
    target 15
    weight 0.073
  ]
  edge
  [
    source 26
    target 16
    weight 0.007
  ]
  edge
  [
    source 26
    target 17
    weight 0.016
  ]
  edge
  [
    source 26
    target 18
    weight 0.193
  ]
  edge
  [
    source 26
    target 19
    weight 0.099
  ]
  edge
  [
    source 26
    target 20
    weight 0.188
  ]
  edge
  [
    source 26
    target 21
    weight 0.313
  ]
  edge
  [
    source 26
    target 22
    weight 0.069
  ]
  edge
  [
    source 26
    target 23
    weight 0.066
  ]
  edge
  [
    source 26
    target 24
    weight 0.001
  ]
  edge
  [
    source 26
    target 25
    weight 0.022
  ]
  edge
  [
    source 26
    target 26
    weight 0
  ]
  edge
  [
    source 26
    target 27
    weight 0.198
  ]
  edge
  [
    source 26
    target 28
    weight 0.278
  ]
  edge
  [
    source 26
    target 29
    weight 0.207
  ]
  edge
  [
    source 26
    target 30
    weight 0.071
  ]
  edge
  [
    source 26
    target 31
    weight 0.253
  ]
  edge
  [
    source 26
    target 32
    weight 0.134
  ]
  edge
  [
    source 26
    target 33
    weight 0.01
  ]
  edge
  [
    source 26
    target 34
    weight 0.002
  ]
  edge
  [
    source 26
    target 35
    weight 0.014
  ]
  edge
  [
    source 26
    target 36
    weight 0.155
  ]
  edge
  [
    source 26
    target 37
    weight 0
  ]
  edge
  [
    source 26
    target 38
    weight 0.127
  ]
  edge
  [
    source 26
    target 39
    weight 0.022
  ]
  edge
  [
    source 27
    target 0
    weight 0.006
  ]
  edge
  [
    source 27
    target 1
    weight 0.075
  ]
  edge
  [
    source 27
    target 2
    weight 0.105
  ]
  edge
  [
    source 27
    target 3
    weight 0.102
  ]
  edge
  [
    source 27
    target 4
    weight 0.139
  ]
  edge
  [
    source 27
    target 5
    weight 0.291
  ]
  edge
  [
    source 27
    target 6
    weight 0.094
  ]
  edge
  [
    source 27
    target 7
    weight 0.434
  ]
  edge
  [
    source 27
    target 8
    weight 0.143
  ]
  edge
  [
    source 27
    target 9
    weight 0.042
  ]
  edge
  [
    source 27
    target 10
    weight 0.21
  ]
  edge
  [
    source 27
    target 11
    weight 0.145
  ]
  edge
  [
    source 27
    target 12
    weight 0.003
  ]
  edge
  [
    source 27
    target 13
    weight 0.003
  ]
  edge
  [
    source 27
    target 14
    weight 0.13
  ]
  edge
  [
    source 27
    target 15
    weight 0.015
  ]
  edge
  [
    source 27
    target 16
    weight 0.152
  ]
  edge
  [
    source 27
    target 17
    weight 0.016
  ]
  edge
  [
    source 27
    target 18
    weight 0.038
  ]
  edge
  [
    source 27
    target 19
    weight 0.036
  ]
  edge
  [
    source 27
    target 20
    weight 0.139
  ]
  edge
  [
    source 27
    target 21
    weight 0.072
  ]
  edge
  [
    source 27
    target 22
    weight 0.043
  ]
  edge
  [
    source 27
    target 23
    weight 0.001
  ]
  edge
  [
    source 27
    target 24
    weight 0.047
  ]
  edge
  [
    source 27
    target 25
    weight 0.019
  ]
  edge
  [
    source 27
    target 26
    weight 0.198
  ]
  edge
  [
    source 27
    target 27
    weight 0
  ]
  edge
  [
    source 27
    target 28
    weight 0.138
  ]
  edge
  [
    source 27
    target 29
    weight 0.296
  ]
  edge
  [
    source 27
    target 30
    weight 0.21
  ]
  edge
  [
    source 27
    target 31
    weight 0.001
  ]
  edge
  [
    source 27
    target 32
    weight 0.014
  ]
  edge
  [
    source 27
    target 33
    weight 0.006
  ]
  edge
  [
    source 27
    target 34
    weight 0.045
  ]
  edge
  [
    source 27
    target 35
    weight 0
  ]
  edge
  [
    source 27
    target 36
    weight 0.186
  ]
  edge
  [
    source 27
    target 37
    weight 0
  ]
  edge
  [
    source 27
    target 38
    weight 0.091
  ]
  edge
  [
    source 27
    target 39
    weight 0.056
  ]
  edge
  [
    source 28
    target 0
    weight 0.231
  ]
  edge
  [
    source 28
    target 1
    weight 0.033
  ]
  edge
  [
    source 28
    target 2
    weight 0.047
  ]
  edge
  [
    source 28
    target 3
    weight 0.003
  ]
  edge
  [
    source 28
    target 4
    weight 0.143
  ]
  edge
  [
    source 28
    target 5
    weight 0.173
  ]
  edge
  [
    source 28
    target 6
    weight 0.049
  ]
  edge
  [
    source 28
    target 7
    weight 0.513
  ]
  edge
  [
    source 28
    target 8
    weight 0.186
  ]
  edge
  [
    source 28
    target 9
    weight 0.077
  ]
  edge
  [
    source 28
    target 10
    weight 0.214
  ]
  edge
  [
    source 28
    target 11
    weight 0.054
  ]
  edge
  [
    source 28
    target 12
    weight 0
  ]
  edge
  [
    source 28
    target 13
    weight 0.003
  ]
  edge
  [
    source 28
    target 14
    weight 0.23
  ]
  edge
  [
    source 28
    target 15
    weight 0.061
  ]
  edge
  [
    source 28
    target 16
    weight 0.173
  ]
  edge
  [
    source 28
    target 17
    weight 0.031
  ]
  edge
  [
    source 28
    target 18
    weight 0.038
  ]
  edge
  [
    source 28
    target 19
    weight 0
  ]
  edge
  [
    source 28
    target 20
    weight 0.144
  ]
  edge
  [
    source 28
    target 21
    weight 0.015
  ]
  edge
  [
    source 28
    target 22
    weight 0.001
  ]
  edge
  [
    source 28
    target 23
    weight 0.041
  ]
  edge
  [
    source 28
    target 24
    weight 0.007
  ]
  edge
  [
    source 28
    target 25
    weight 0.038
  ]
  edge
  [
    source 28
    target 26
    weight 0.278
  ]
  edge
  [
    source 28
    target 27
    weight 0.138
  ]
  edge
  [
    source 28
    target 28
    weight 0
  ]
  edge
  [
    source 28
    target 29
    weight 0.308
  ]
  edge
  [
    source 28
    target 30
    weight 0.323
  ]
  edge
  [
    source 28
    target 31
    weight 0.024
  ]
  edge
  [
    source 28
    target 32
    weight 0.026
  ]
  edge
  [
    source 28
    target 33
    weight 0.011
  ]
  edge
  [
    source 28
    target 34
    weight 0.14
  ]
  edge
  [
    source 28
    target 35
    weight 0.008
  ]
  edge
  [
    source 28
    target 36
    weight 0.279
  ]
  edge
  [
    source 28
    target 37
    weight 0.013
  ]
  edge
  [
    source 28
    target 38
    weight 0.011
  ]
  edge
  [
    source 28
    target 39
    weight 0.065
  ]
  edge
  [
    source 29
    target 0
    weight 0.149
  ]
  edge
  [
    source 29
    target 1
    weight 0.167
  ]
  edge
  [
    source 29
    target 2
    weight 0.103
  ]
  edge
  [
    source 29
    target 3
    weight 0.232
  ]
  edge
  [
    source 29
    target 4
    weight 0.142
  ]
  edge
  [
    source 29
    target 5
    weight 0.109
  ]
  edge
  [
    source 29
    target 6
    weight 0.171
  ]
  edge
  [
    source 29
    target 7
    weight 0.2
  ]
  edge
  [
    source 29
    target 8
    weight 0.006
  ]
  edge
  [
    source 29
    target 9
    weight 0.048
  ]
  edge
  [
    source 29
    target 10
    weight 0.42
  ]
  edge
  [
    source 29
    target 11
    weight 0.356
  ]
  edge
  [
    source 29
    target 12
    weight 0.004
  ]
  edge
  [
    source 29
    target 13
    weight 0.009
  ]
  edge
  [
    source 29
    target 14
    weight 0.064
  ]
  edge
  [
    source 29
    target 15
    weight 0.018
  ]
  edge
  [
    source 29
    target 16
    weight 0.272
  ]
  edge
  [
    source 29
    target 17
    weight 0.209
  ]
  edge
  [
    source 29
    target 18
    weight 0.117
  ]
  edge
  [
    source 29
    target 19
    weight 0.074
  ]
  edge
  [
    source 29
    target 20
    weight 0.132
  ]
  edge
  [
    source 29
    target 21
    weight 0.052
  ]
  edge
  [
    source 29
    target 22
    weight 0.104
  ]
  edge
  [
    source 29
    target 23
    weight 0.198
  ]
  edge
  [
    source 29
    target 24
    weight 0.072
  ]
  edge
  [
    source 29
    target 25
    weight 0.334
  ]
  edge
  [
    source 29
    target 26
    weight 0.207
  ]
  edge
  [
    source 29
    target 27
    weight 0.296
  ]
  edge
  [
    source 29
    target 28
    weight 0.308
  ]
  edge
  [
    source 29
    target 29
    weight 0
  ]
  edge
  [
    source 29
    target 30
    weight 0.791
  ]
  edge
  [
    source 29
    target 31
    weight 0.002
  ]
  edge
  [
    source 29
    target 32
    weight 0.123
  ]
  edge
  [
    source 29
    target 33
    weight 0.366
  ]
  edge
  [
    source 29
    target 34
    weight 0.253
  ]
  edge
  [
    source 29
    target 35
    weight 0.018
  ]
  edge
  [
    source 29
    target 36
    weight 0.056
  ]
  edge
  [
    source 29
    target 37
    weight 0
  ]
  edge
  [
    source 29
    target 38
    weight 0.143
  ]
  edge
  [
    source 29
    target 39
    weight 0.01
  ]
  edge
  [
    source 30
    target 0
    weight 0.05
  ]
  edge
  [
    source 30
    target 1
    weight 0.001
  ]
  edge
  [
    source 30
    target 2
    weight 0
  ]
  edge
  [
    source 30
    target 3
    weight 0.067
  ]
  edge
  [
    source 30
    target 4
    weight 0.23
  ]
  edge
  [
    source 30
    target 5
    weight 0.047
  ]
  edge
  [
    source 30
    target 6
    weight 0.066
  ]
  edge
  [
    source 30
    target 7
    weight 0.242
  ]
  edge
  [
    source 30
    target 8
    weight 0
  ]
  edge
  [
    source 30
    target 9
    weight 0.061
  ]
  edge
  [
    source 30
    target 10
    weight 0.208
  ]
  edge
  [
    source 30
    target 11
    weight 0.391
  ]
  edge
  [
    source 30
    target 12
    weight 0.003
  ]
  edge
  [
    source 30
    target 13
    weight 0.025
  ]
  edge
  [
    source 30
    target 14
    weight 0.01
  ]
  edge
  [
    source 30
    target 15
    weight 0.006
  ]
  edge
  [
    source 30
    target 16
    weight 0.428
  ]
  edge
  [
    source 30
    target 17
    weight 0.238
  ]
  edge
  [
    source 30
    target 18
    weight 0.03
  ]
  edge
  [
    source 30
    target 19
    weight 0.115
  ]
  edge
  [
    source 30
    target 20
    weight 0.133
  ]
  edge
  [
    source 30
    target 21
    weight 0.043
  ]
  edge
  [
    source 30
    target 22
    weight 0.032
  ]
  edge
  [
    source 30
    target 23
    weight 0.162
  ]
  edge
  [
    source 30
    target 24
    weight 0.103
  ]
  edge
  [
    source 30
    target 25
    weight 0.323
  ]
  edge
  [
    source 30
    target 26
    weight 0.071
  ]
  edge
  [
    source 30
    target 27
    weight 0.21
  ]
  edge
  [
    source 30
    target 28
    weight 0.323
  ]
  edge
  [
    source 30
    target 29
    weight 0.791
  ]
  edge
  [
    source 30
    target 30
    weight 0
  ]
  edge
  [
    source 30
    target 31
    weight 0.003
  ]
  edge
  [
    source 30
    target 32
    weight 0.088
  ]
  edge
  [
    source 30
    target 33
    weight 0.246
  ]
  edge
  [
    source 30
    target 34
    weight 0.336
  ]
  edge
  [
    source 30
    target 35
    weight 0
  ]
  edge
  [
    source 30
    target 36
    weight 0.028
  ]
  edge
  [
    source 30
    target 37
    weight 0
  ]
  edge
  [
    source 30
    target 38
    weight 0.295
  ]
  edge
  [
    source 30
    target 39
    weight 0.024
  ]
  edge
  [
    source 31
    target 0
    weight 0.051
  ]
  edge
  [
    source 31
    target 1
    weight 0.012
  ]
  edge
  [
    source 31
    target 2
    weight 0.002
  ]
  edge
  [
    source 31
    target 3
    weight 0
  ]
  edge
  [
    source 31
    target 4
    weight 0.211
  ]
  edge
  [
    source 31
    target 5
    weight 0.195
  ]
  edge
  [
    source 31
    target 6
    weight 0.002
  ]
  edge
  [
    source 31
    target 7
    weight 0.005
  ]
  edge
  [
    source 31
    target 8
    weight 0.077
  ]
  edge
  [
    source 31
    target 9
    weight 0.138
  ]
  edge
  [
    source 31
    target 10
    weight 0.07
  ]
  edge
  [
    source 31
    target 11
    weight 0.025
  ]
  edge
  [
    source 31
    target 12
    weight 0.209
  ]
  edge
  [
    source 31
    target 13
    weight 0.126
  ]
  edge
  [
    source 31
    target 14
    weight 0
  ]
  edge
  [
    source 31
    target 15
    weight 0.003
  ]
  edge
  [
    source 31
    target 16
    weight 0.243
  ]
  edge
  [
    source 31
    target 17
    weight 0.089
  ]
  edge
  [
    source 31
    target 18
    weight 0.022
  ]
  edge
  [
    source 31
    target 19
    weight 0.021
  ]
  edge
  [
    source 31
    target 20
    weight 0.105
  ]
  edge
  [
    source 31
    target 21
    weight 0.138
  ]
  edge
  [
    source 31
    target 22
    weight 0.018
  ]
  edge
  [
    source 31
    target 23
    weight 0.132
  ]
  edge
  [
    source 31
    target 24
    weight 0.031
  ]
  edge
  [
    source 31
    target 25
    weight 0.128
  ]
  edge
  [
    source 31
    target 26
    weight 0.253
  ]
  edge
  [
    source 31
    target 27
    weight 0.001
  ]
  edge
  [
    source 31
    target 28
    weight 0.024
  ]
  edge
  [
    source 31
    target 29
    weight 0.002
  ]
  edge
  [
    source 31
    target 30
    weight 0.003
  ]
  edge
  [
    source 31
    target 31
    weight 0
  ]
  edge
  [
    source 31
    target 32
    weight 0.003
  ]
  edge
  [
    source 31
    target 33
    weight 0.005
  ]
  edge
  [
    source 31
    target 34
    weight 0.003
  ]
  edge
  [
    source 31
    target 35
    weight 0.112
  ]
  edge
  [
    source 31
    target 36
    weight 0.003
  ]
  edge
  [
    source 31
    target 37
    weight 0.003
  ]
  edge
  [
    source 31
    target 38
    weight 0.149
  ]
  edge
  [
    source 31
    target 39
    weight 0.016
  ]
  edge
  [
    source 32
    target 0
    weight 0.001
  ]
  edge
  [
    source 32
    target 1
    weight 0.018
  ]
  edge
  [
    source 32
    target 2
    weight 0.028
  ]
  edge
  [
    source 32
    target 3
    weight 0.034
  ]
  edge
  [
    source 32
    target 4
    weight 0.195
  ]
  edge
  [
    source 32
    target 5
    weight 0.435
  ]
  edge
  [
    source 32
    target 6
    weight 0.015
  ]
  edge
  [
    source 32
    target 7
    weight 0.012
  ]
  edge
  [
    source 32
    target 8
    weight 0.205
  ]
  edge
  [
    source 32
    target 9
    weight 0.31
  ]
  edge
  [
    source 32
    target 10
    weight 0.114
  ]
  edge
  [
    source 32
    target 11
    weight 0.012
  ]
  edge
  [
    source 32
    target 12
    weight 0.023
  ]
  edge
  [
    source 32
    target 13
    weight 0.036
  ]
  edge
  [
    source 32
    target 14
    weight 0.194
  ]
  edge
  [
    source 32
    target 15
    weight 0.154
  ]
  edge
  [
    source 32
    target 16
    weight 0.139
  ]
  edge
  [
    source 32
    target 17
    weight 0.126
  ]
  edge
  [
    source 32
    target 18
    weight 0.042
  ]
  edge
  [
    source 32
    target 19
    weight 0.005
  ]
  edge
  [
    source 32
    target 20
    weight 0.14
  ]
  edge
  [
    source 32
    target 21
    weight 0.203
  ]
  edge
  [
    source 32
    target 22
    weight 0.11
  ]
  edge
  [
    source 32
    target 23
    weight 0.11
  ]
  edge
  [
    source 32
    target 24
    weight 0.035
  ]
  edge
  [
    source 32
    target 25
    weight 0.062
  ]
  edge
  [
    source 32
    target 26
    weight 0.134
  ]
  edge
  [
    source 32
    target 27
    weight 0.014
  ]
  edge
  [
    source 32
    target 28
    weight 0.026
  ]
  edge
  [
    source 32
    target 29
    weight 0.123
  ]
  edge
  [
    source 32
    target 30
    weight 0.088
  ]
  edge
  [
    source 32
    target 31
    weight 0.003
  ]
  edge
  [
    source 32
    target 32
    weight 0
  ]
  edge
  [
    source 32
    target 33
    weight 0.035
  ]
  edge
  [
    source 32
    target 34
    weight 0.001
  ]
  edge
  [
    source 32
    target 35
    weight 0.081
  ]
  edge
  [
    source 32
    target 36
    weight 0.038
  ]
  edge
  [
    source 32
    target 37
    weight 0.055
  ]
  edge
  [
    source 32
    target 38
    weight 0.057
  ]
  edge
  [
    source 32
    target 39
    weight 0.021
  ]
  edge
  [
    source 33
    target 0
    weight 0.166
  ]
  edge
  [
    source 33
    target 1
    weight 0.194
  ]
  edge
  [
    source 33
    target 2
    weight 0.12
  ]
  edge
  [
    source 33
    target 3
    weight 0.141
  ]
  edge
  [
    source 33
    target 4
    weight 0.009
  ]
  edge
  [
    source 33
    target 5
    weight 0.013
  ]
  edge
  [
    source 33
    target 6
    weight 0.034
  ]
  edge
  [
    source 33
    target 7
    weight 0.058
  ]
  edge
  [
    source 33
    target 8
    weight 0.001
  ]
  edge
  [
    source 33
    target 9
    weight 0
  ]
  edge
  [
    source 33
    target 10
    weight 0.248
  ]
  edge
  [
    source 33
    target 11
    weight 0.234
  ]
  edge
  [
    source 33
    target 12
    weight 0.03
  ]
  edge
  [
    source 33
    target 13
    weight 0.045
  ]
  edge
  [
    source 33
    target 14
    weight 0.03
  ]
  edge
  [
    source 33
    target 15
    weight 0.041
  ]
  edge
  [
    source 33
    target 16
    weight 0.008
  ]
  edge
  [
    source 33
    target 17
    weight 0.018
  ]
  edge
  [
    source 33
    target 18
    weight 0.024
  ]
  edge
  [
    source 33
    target 19
    weight 0.068
  ]
  edge
  [
    source 33
    target 20
    weight 0.01
  ]
  edge
  [
    source 33
    target 21
    weight 0.034
  ]
  edge
  [
    source 33
    target 22
    weight 0.011
  ]
  edge
  [
    source 33
    target 23
    weight 0.164
  ]
  edge
  [
    source 33
    target 24
    weight 0.088
  ]
  edge
  [
    source 33
    target 25
    weight 0.104
  ]
  edge
  [
    source 33
    target 26
    weight 0.01
  ]
  edge
  [
    source 33
    target 27
    weight 0.006
  ]
  edge
  [
    source 33
    target 28
    weight 0.011
  ]
  edge
  [
    source 33
    target 29
    weight 0.366
  ]
  edge
  [
    source 33
    target 30
    weight 0.246
  ]
  edge
  [
    source 33
    target 31
    weight 0.005
  ]
  edge
  [
    source 33
    target 32
    weight 0.035
  ]
  edge
  [
    source 33
    target 33
    weight 0
  ]
  edge
  [
    source 33
    target 34
    weight 0.003
  ]
  edge
  [
    source 33
    target 35
    weight 0.065
  ]
  edge
  [
    source 33
    target 36
    weight 0.01
  ]
  edge
  [
    source 33
    target 37
    weight 0.025
  ]
  edge
  [
    source 33
    target 38
    weight 0.079
  ]
  edge
  [
    source 33
    target 39
    weight 0.002
  ]
  edge
  [
    source 34
    target 0
    weight 0.095
  ]
  edge
  [
    source 34
    target 1
    weight 0.252
  ]
  edge
  [
    source 34
    target 2
    weight 0.211
  ]
  edge
  [
    source 34
    target 3
    weight 0.404
  ]
  edge
  [
    source 34
    target 4
    weight 0.019
  ]
  edge
  [
    source 34
    target 5
    weight 0
  ]
  edge
  [
    source 34
    target 6
    weight 0.068
  ]
  edge
  [
    source 34
    target 7
    weight 0.079
  ]
  edge
  [
    source 34
    target 8
    weight 0.003
  ]
  edge
  [
    source 34
    target 9
    weight 0.009
  ]
  edge
  [
    source 34
    target 10
    weight 0.242
  ]
  edge
  [
    source 34
    target 11
    weight 0.16
  ]
  edge
  [
    source 34
    target 12
    weight 0
  ]
  edge
  [
    source 34
    target 13
    weight 0
  ]
  edge
  [
    source 34
    target 14
    weight 0.022
  ]
  edge
  [
    source 34
    target 15
    weight 0.039
  ]
  edge
  [
    source 34
    target 16
    weight 0
  ]
  edge
  [
    source 34
    target 17
    weight 0.015
  ]
  edge
  [
    source 34
    target 18
    weight 0.037
  ]
  edge
  [
    source 34
    target 19
    weight 0.1
  ]
  edge
  [
    source 34
    target 20
    weight 0.006
  ]
  edge
  [
    source 34
    target 21
    weight 0.025
  ]
  edge
  [
    source 34
    target 22
    weight 0.143
  ]
  edge
  [
    source 34
    target 23
    weight 0.255
  ]
  edge
  [
    source 34
    target 24
    weight 0.054
  ]
  edge
  [
    source 34
    target 25
    weight 0.109
  ]
  edge
  [
    source 34
    target 26
    weight 0.002
  ]
  edge
  [
    source 34
    target 27
    weight 0.045
  ]
  edge
  [
    source 34
    target 28
    weight 0.14
  ]
  edge
  [
    source 34
    target 29
    weight 0.253
  ]
  edge
  [
    source 34
    target 30
    weight 0.336
  ]
  edge
  [
    source 34
    target 31
    weight 0.003
  ]
  edge
  [
    source 34
    target 32
    weight 0.001
  ]
  edge
  [
    source 34
    target 33
    weight 0.003
  ]
  edge
  [
    source 34
    target 34
    weight 0
  ]
  edge
  [
    source 34
    target 35
    weight 0
  ]
  edge
  [
    source 34
    target 36
    weight 0.248
  ]
  edge
  [
    source 34
    target 37
    weight 0
  ]
  edge
  [
    source 34
    target 38
    weight 0.106
  ]
  edge
  [
    source 34
    target 39
    weight 0.194
  ]
  edge
  [
    source 35
    target 0
    weight 0.046
  ]
  edge
  [
    source 35
    target 1
    weight 0
  ]
  edge
  [
    source 35
    target 2
    weight 0.002
  ]
  edge
  [
    source 35
    target 3
    weight 0.034
  ]
  edge
  [
    source 35
    target 4
    weight 0.009
  ]
  edge
  [
    source 35
    target 5
    weight 0.044
  ]
  edge
  [
    source 35
    target 6
    weight 0.006
  ]
  edge
  [
    source 35
    target 7
    weight 0.003
  ]
  edge
  [
    source 35
    target 8
    weight 0.116
  ]
  edge
  [
    source 35
    target 9
    weight 0.067
  ]
  edge
  [
    source 35
    target 10
    weight 0.063
  ]
  edge
  [
    source 35
    target 11
    weight 0.041
  ]
  edge
  [
    source 35
    target 12
    weight 0.005
  ]
  edge
  [
    source 35
    target 13
    weight 0.001
  ]
  edge
  [
    source 35
    target 14
    weight 0
  ]
  edge
  [
    source 35
    target 15
    weight 0.068
  ]
  edge
  [
    source 35
    target 16
    weight 0.015
  ]
  edge
  [
    source 35
    target 17
    weight 0.013
  ]
  edge
  [
    source 35
    target 18
    weight 0.003
  ]
  edge
  [
    source 35
    target 19
    weight 0.001
  ]
  edge
  [
    source 35
    target 20
    weight 0.003
  ]
  edge
  [
    source 35
    target 21
    weight 0.156
  ]
  edge
  [
    source 35
    target 22
    weight 0.005
  ]
  edge
  [
    source 35
    target 23
    weight 0.001
  ]
  edge
  [
    source 35
    target 24
    weight 0.052
  ]
  edge
  [
    source 35
    target 25
    weight 0.016
  ]
  edge
  [
    source 35
    target 26
    weight 0.014
  ]
  edge
  [
    source 35
    target 27
    weight 0
  ]
  edge
  [
    source 35
    target 28
    weight 0.008
  ]
  edge
  [
    source 35
    target 29
    weight 0.018
  ]
  edge
  [
    source 35
    target 30
    weight 0
  ]
  edge
  [
    source 35
    target 31
    weight 0.112
  ]
  edge
  [
    source 35
    target 32
    weight 0.081
  ]
  edge
  [
    source 35
    target 33
    weight 0.065
  ]
  edge
  [
    source 35
    target 34
    weight 0
  ]
  edge
  [
    source 35
    target 35
    weight 0
  ]
  edge
  [
    source 35
    target 36
    weight 0.16
  ]
  edge
  [
    source 35
    target 37
    weight 0.018
  ]
  edge
  [
    source 35
    target 38
    weight 0.149
  ]
  edge
  [
    source 35
    target 39
    weight 0.04
  ]
  edge
  [
    source 36
    target 0
    weight 0.044
  ]
  edge
  [
    source 36
    target 1
    weight 0.002
  ]
  edge
  [
    source 36
    target 2
    weight 0.015
  ]
  edge
  [
    source 36
    target 3
    weight 0.022
  ]
  edge
  [
    source 36
    target 4
    weight 0.12
  ]
  edge
  [
    source 36
    target 5
    weight 0.13
  ]
  edge
  [
    source 36
    target 6
    weight 0
  ]
  edge
  [
    source 36
    target 7
    weight 0.002
  ]
  edge
  [
    source 36
    target 8
    weight 0.03
  ]
  edge
  [
    source 36
    target 9
    weight 0.067
  ]
  edge
  [
    source 36
    target 10
    weight 0.044
  ]
  edge
  [
    source 36
    target 11
    weight 0.114
  ]
  edge
  [
    source 36
    target 12
    weight 0.003
  ]
  edge
  [
    source 36
    target 13
    weight 0.018
  ]
  edge
  [
    source 36
    target 14
    weight 0.046
  ]
  edge
  [
    source 36
    target 15
    weight 0.126
  ]
  edge
  [
    source 36
    target 16
    weight 0.177
  ]
  edge
  [
    source 36
    target 17
    weight 0.091
  ]
  edge
  [
    source 36
    target 18
    weight 0.012
  ]
  edge
  [
    source 36
    target 19
    weight 0.005
  ]
  edge
  [
    source 36
    target 20
    weight 0
  ]
  edge
  [
    source 36
    target 21
    weight 0.082
  ]
  edge
  [
    source 36
    target 22
    weight 0
  ]
  edge
  [
    source 36
    target 23
    weight 0
  ]
  edge
  [
    source 36
    target 24
    weight 0.01
  ]
  edge
  [
    source 36
    target 25
    weight 0.009
  ]
  edge
  [
    source 36
    target 26
    weight 0.155
  ]
  edge
  [
    source 36
    target 27
    weight 0.186
  ]
  edge
  [
    source 36
    target 28
    weight 0.279
  ]
  edge
  [
    source 36
    target 29
    weight 0.056
  ]
  edge
  [
    source 36
    target 30
    weight 0.028
  ]
  edge
  [
    source 36
    target 31
    weight 0.003
  ]
  edge
  [
    source 36
    target 32
    weight 0.038
  ]
  edge
  [
    source 36
    target 33
    weight 0.01
  ]
  edge
  [
    source 36
    target 34
    weight 0.248
  ]
  edge
  [
    source 36
    target 35
    weight 0.16
  ]
  edge
  [
    source 36
    target 36
    weight 0
  ]
  edge
  [
    source 36
    target 37
    weight 0.047
  ]
  edge
  [
    source 36
    target 38
    weight 0.065
  ]
  edge
  [
    source 36
    target 39
    weight 0.086
  ]
  edge
  [
    source 37
    target 0
    weight 0.009
  ]
  edge
  [
    source 37
    target 1
    weight 0.019
  ]
  edge
  [
    source 37
    target 2
    weight 0.011
  ]
  edge
  [
    source 37
    target 3
    weight 0.012
  ]
  edge
  [
    source 37
    target 4
    weight 0.005
  ]
  edge
  [
    source 37
    target 5
    weight 0.001
  ]
  edge
  [
    source 37
    target 6
    weight 0.022
  ]
  edge
  [
    source 37
    target 7
    weight 0.004
  ]
  edge
  [
    source 37
    target 8
    weight 0.019
  ]
  edge
  [
    source 37
    target 9
    weight 0.009
  ]
  edge
  [
    source 37
    target 10
    weight 0.005
  ]
  edge
  [
    source 37
    target 11
    weight 0.009
  ]
  edge
  [
    source 37
    target 12
    weight 0.045
  ]
  edge
  [
    source 37
    target 13
    weight 0.056
  ]
  edge
  [
    source 37
    target 14
    weight 0.001
  ]
  edge
  [
    source 37
    target 15
    weight 0
  ]
  edge
  [
    source 37
    target 16
    weight 0.017
  ]
  edge
  [
    source 37
    target 17
    weight 0
  ]
  edge
  [
    source 37
    target 18
    weight 0.008
  ]
  edge
  [
    source 37
    target 19
    weight 0.013
  ]
  edge
  [
    source 37
    target 20
    weight 0.036
  ]
  edge
  [
    source 37
    target 21
    weight 0.015
  ]
  edge
  [
    source 37
    target 22
    weight 0.001
  ]
  edge
  [
    source 37
    target 23
    weight 0.012
  ]
  edge
  [
    source 37
    target 24
    weight 0.036
  ]
  edge
  [
    source 37
    target 25
    weight 0.01
  ]
  edge
  [
    source 37
    target 26
    weight 0
  ]
  edge
  [
    source 37
    target 27
    weight 0
  ]
  edge
  [
    source 37
    target 28
    weight 0.013
  ]
  edge
  [
    source 37
    target 29
    weight 0
  ]
  edge
  [
    source 37
    target 30
    weight 0
  ]
  edge
  [
    source 37
    target 31
    weight 0.003
  ]
  edge
  [
    source 37
    target 32
    weight 0.055
  ]
  edge
  [
    source 37
    target 33
    weight 0.025
  ]
  edge
  [
    source 37
    target 34
    weight 0
  ]
  edge
  [
    source 37
    target 35
    weight 0.018
  ]
  edge
  [
    source 37
    target 36
    weight 0.047
  ]
  edge
  [
    source 37
    target 37
    weight 0
  ]
  edge
  [
    source 37
    target 38
    weight 0.001
  ]
  edge
  [
    source 37
    target 39
    weight 0.016
  ]
  edge
  [
    source 38
    target 0
    weight 0.104
  ]
  edge
  [
    source 38
    target 1
    weight 0.173
  ]
  edge
  [
    source 38
    target 2
    weight 0.451
  ]
  edge
  [
    source 38
    target 3
    weight 0.11
  ]
  edge
  [
    source 38
    target 4
    weight 0.335
  ]
  edge
  [
    source 38
    target 5
    weight 0.331
  ]
  edge
  [
    source 38
    target 6
    weight 0.039
  ]
  edge
  [
    source 38
    target 7
    weight 0
  ]
  edge
  [
    source 38
    target 8
    weight 0.303
  ]
  edge
  [
    source 38
    target 9
    weight 0.214
  ]
  edge
  [
    source 38
    target 10
    weight 0.161
  ]
  edge
  [
    source 38
    target 11
    weight 0.047
  ]
  edge
  [
    source 38
    target 12
    weight 0.055
  ]
  edge
  [
    source 38
    target 13
    weight 0.114
  ]
  edge
  [
    source 38
    target 14
    weight 0.48
  ]
  edge
  [
    source 38
    target 15
    weight 0.278
  ]
  edge
  [
    source 38
    target 16
    weight 0.288
  ]
  edge
  [
    source 38
    target 17
    weight 0.033
  ]
  edge
  [
    source 38
    target 18
    weight 0.249
  ]
  edge
  [
    source 38
    target 19
    weight 0.086
  ]
  edge
  [
    source 38
    target 20
    weight 0.205
  ]
  edge
  [
    source 38
    target 21
    weight 0.199
  ]
  edge
  [
    source 38
    target 22
    weight 0.161
  ]
  edge
  [
    source 38
    target 23
    weight 0.189
  ]
  edge
  [
    source 38
    target 24
    weight 0.322
  ]
  edge
  [
    source 38
    target 25
    weight 0.116
  ]
  edge
  [
    source 38
    target 26
    weight 0.127
  ]
  edge
  [
    source 38
    target 27
    weight 0.091
  ]
  edge
  [
    source 38
    target 28
    weight 0.011
  ]
  edge
  [
    source 38
    target 29
    weight 0.143
  ]
  edge
  [
    source 38
    target 30
    weight 0.295
  ]
  edge
  [
    source 38
    target 31
    weight 0.149
  ]
  edge
  [
    source 38
    target 32
    weight 0.057
  ]
  edge
  [
    source 38
    target 33
    weight 0.079
  ]
  edge
  [
    source 38
    target 34
    weight 0.106
  ]
  edge
  [
    source 38
    target 35
    weight 0.149
  ]
  edge
  [
    source 38
    target 36
    weight 0.065
  ]
  edge
  [
    source 38
    target 37
    weight 0.001
  ]
  edge
  [
    source 38
    target 38
    weight 0
  ]
  edge
  [
    source 38
    target 39
    weight 0.069
  ]
  edge
  [
    source 39
    target 0
    weight 0
  ]
  edge
  [
    source 39
    target 1
    weight 0.009
  ]
  edge
  [
    source 39
    target 2
    weight 0.06
  ]
  edge
  [
    source 39
    target 3
    weight 0.081
  ]
  edge
  [
    source 39
    target 4
    weight 0.011
  ]
  edge
  [
    source 39
    target 5
    weight 0.025
  ]
  edge
  [
    source 39
    target 6
    weight 0.05
  ]
  edge
  [
    source 39
    target 7
    weight 0
  ]
  edge
  [
    source 39
    target 8
    weight 0.001
  ]
  edge
  [
    source 39
    target 9
    weight 0.004
  ]
  edge
  [
    source 39
    target 10
    weight 0.022
  ]
  edge
  [
    source 39
    target 11
    weight 0.042
  ]
  edge
  [
    source 39
    target 12
    weight 0.003
  ]
  edge
  [
    source 39
    target 13
    weight 0.035
  ]
  edge
  [
    source 39
    target 14
    weight 0.058
  ]
  edge
  [
    source 39
    target 15
    weight 0.01
  ]
  edge
  [
    source 39
    target 16
    weight 0.017
  ]
  edge
  [
    source 39
    target 17
    weight 0.036
  ]
  edge
  [
    source 39
    target 18
    weight 0.015
  ]
  edge
  [
    source 39
    target 19
    weight 0.026
  ]
  edge
  [
    source 39
    target 20
    weight 0.032
  ]
  edge
  [
    source 39
    target 21
    weight 0.015
  ]
  edge
  [
    source 39
    target 22
    weight 0.005
  ]
  edge
  [
    source 39
    target 23
    weight 0.037
  ]
  edge
  [
    source 39
    target 24
    weight 0.002
  ]
  edge
  [
    source 39
    target 25
    weight 0.033
  ]
  edge
  [
    source 39
    target 26
    weight 0.022
  ]
  edge
  [
    source 39
    target 27
    weight 0.056
  ]
  edge
  [
    source 39
    target 28
    weight 0.065
  ]
  edge
  [
    source 39
    target 29
    weight 0.01
  ]
  edge
  [
    source 39
    target 30
    weight 0.024
  ]
  edge
  [
    source 39
    target 31
    weight 0.016
  ]
  edge
  [
    source 39
    target 32
    weight 0.021
  ]
  edge
  [
    source 39
    target 33
    weight 0.002
  ]
  edge
  [
    source 39
    target 34
    weight 0.194
  ]
  edge
  [
    source 39
    target 35
    weight 0.04
  ]
  edge
  [
    source 39
    target 36
    weight 0.086
  ]
  edge
  [
    source 39
    target 37
    weight 0.016
  ]
  edge
  [
    source 39
    target 38
    weight 0.069
  ]
  edge
  [
    source 39
    target 39
    weight 0
  ]
]
