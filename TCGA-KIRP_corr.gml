Creator "igraph version 1.2.2 Wed Jul 24 12:20:26 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "c.1p"
  ]
  node
  [
    id 1
    name "c.1q"
  ]
  node
  [
    id 2
    name "c.2p"
  ]
  node
  [
    id 3
    name "c.2q"
  ]
  node
  [
    id 4
    name "c.3p"
  ]
  node
  [
    id 5
    name "c.3q"
  ]
  node
  [
    id 6
    name "c.4p"
  ]
  node
  [
    id 7
    name "c.4q"
  ]
  node
  [
    id 8
    name "c.5p"
  ]
  node
  [
    id 9
    name "c.5q"
  ]
  node
  [
    id 10
    name "c.6p"
  ]
  node
  [
    id 11
    name "c.6q"
  ]
  node
  [
    id 12
    name "c.7p"
  ]
  node
  [
    id 13
    name "c.7q"
  ]
  node
  [
    id 14
    name "c.8q"
  ]
  node
  [
    id 15
    name "c.9p"
  ]
  node
  [
    id 16
    name "c.10p"
  ]
  node
  [
    id 17
    name "c.10q"
  ]
  node
  [
    id 18
    name "c.11p"
  ]
  node
  [
    id 19
    name "c.11q"
  ]
  node
  [
    id 20
    name "c.12p"
  ]
  node
  [
    id 21
    name "c.12q"
  ]
  node
  [
    id 22
    name "c.13q"
  ]
  node
  [
    id 23
    name "c.14q"
  ]
  node
  [
    id 24
    name "c.15q"
  ]
  node
  [
    id 25
    name "c.16p"
  ]
  node
  [
    id 26
    name "c.16q"
  ]
  node
  [
    id 27
    name "c.17p"
  ]
  node
  [
    id 28
    name "c.17q"
  ]
  node
  [
    id 29
    name "c.18q"
  ]
  node
  [
    id 30
    name "c.19p"
  ]
  node
  [
    id 31
    name "c.19q"
  ]
  node
  [
    id 32
    name "c.20p"
  ]
  node
  [
    id 33
    name "c.20q"
  ]
  edge
  [
    source 0
    target 10
    weight 0.254
    weight2 -0.504
  ]
  edge
  [
    source 0
    target 11
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 0
    target 15
    weight 0.212
    weight2 -0.46
  ]
  edge
  [
    source 0
    target 17
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 0
    target 23
    weight 0.266
    weight2 -0.515
  ]
  edge
  [
    source 0
    target 30
    weight 0.269
    weight2 -0.519
  ]
  edge
  [
    source 1
    target 4
    weight 0.202
    weight2 -0.449
  ]
  edge
  [
    source 1
    target 6
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 1
    target 7
    weight 0.229
    weight2 -0.478
  ]
  edge
  [
    source 1
    target 15
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 1
    target 18
    weight 0.19
    weight2 -0.436
  ]
  edge
  [
    source 1
    target 19
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 1
    target 29
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 1
    target 31
    weight 0.24
    weight2 -0.489
  ]
  edge
  [
    source 2
    target 4
    weight 0.194
    weight2 -0.441
  ]
  edge
  [
    source 2
    target 6
    weight 0.263
    weight2 -0.513
  ]
  edge
  [
    source 2
    target 9
    weight 0.222
    weight2 -0.471
  ]
  edge
  [
    source 2
    target 14
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 2
    target 19
    weight 0.167
    weight2 -0.409
  ]
  edge
  [
    source 2
    target 22
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 2
    target 24
    weight 0.174
    weight2 -0.418
  ]
  edge
  [
    source 3
    target 4
    weight 0.357
    weight2 -0.597
  ]
  edge
  [
    source 3
    target 5
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 3
    target 14
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 3
    target 22
    weight 0.26
    weight2 -0.51
  ]
  edge
  [
    source 4
    target 1
    weight 0.202
    weight2 -0.449
  ]
  edge
  [
    source 4
    target 2
    weight 0.194
    weight2 -0.441
  ]
  edge
  [
    source 4
    target 3
    weight 0.357
    weight2 -0.597
  ]
  edge
  [
    source 4
    target 5
    weight 0.18
    weight2 0.424
  ]
  edge
  [
    source 4
    target 6
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 4
    target 8
    weight 0.227
    weight2 -0.476
  ]
  edge
  [
    source 4
    target 9
    weight 0.403
    weight2 -0.635
  ]
  edge
  [
    source 4
    target 14
    weight 0.299
    weight2 -0.547
  ]
  edge
  [
    source 4
    target 20
    weight 0.26
    weight2 -0.51
  ]
  edge
  [
    source 4
    target 21
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 4
    target 31
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 5
    target 3
    weight 0.375
    weight2 -0.612
  ]
  edge
  [
    source 5
    target 4
    weight 0.18
    weight2 0.424
  ]
  edge
  [
    source 5
    target 9
    weight 0.315
    weight2 -0.562
  ]
  edge
  [
    source 5
    target 14
    weight 0.215
    weight2 -0.463
  ]
  edge
  [
    source 5
    target 20
    weight 0.385
    weight2 -0.62
  ]
  edge
  [
    source 5
    target 21
    weight 0.407
    weight2 -0.638
  ]
  edge
  [
    source 5
    target 32
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 5
    target 33
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 6
    target 1
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 6
    target 2
    weight 0.263
    weight2 -0.513
  ]
  edge
  [
    source 6
    target 4
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 6
    target 10
    weight 0.324
    weight2 -0.569
  ]
  edge
  [
    source 6
    target 11
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 6
    target 17
    weight 0.32
    weight2 -0.566
  ]
  edge
  [
    source 6
    target 18
    weight 0.278
    weight2 -0.527
  ]
  edge
  [
    source 6
    target 19
    weight 0.309
    weight2 -0.556
  ]
  edge
  [
    source 6
    target 24
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 6
    target 31
    weight 0.276
    weight2 -0.525
  ]
  edge
  [
    source 7
    target 1
    weight 0.229
    weight2 -0.478
  ]
  edge
  [
    source 7
    target 8
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 7
    target 11
    weight 0.275
    weight2 -0.524
  ]
  edge
  [
    source 7
    target 17
    weight 0.234
    weight2 -0.484
  ]
  edge
  [
    source 7
    target 19
    weight 0.231
    weight2 -0.48
  ]
  edge
  [
    source 7
    target 31
    weight 0.269
    weight2 -0.518
  ]
  edge
  [
    source 8
    target 4
    weight 0.227
    weight2 -0.476
  ]
  edge
  [
    source 8
    target 7
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 9
    target 2
    weight 0.222
    weight2 -0.471
  ]
  edge
  [
    source 9
    target 4
    weight 0.403
    weight2 -0.635
  ]
  edge
  [
    source 9
    target 5
    weight 0.315
    weight2 -0.562
  ]
  edge
  [
    source 10
    target 0
    weight 0.254
    weight2 -0.504
  ]
  edge
  [
    source 10
    target 6
    weight 0.324
    weight2 -0.569
  ]
  edge
  [
    source 10
    target 16
    weight 0.203
    weight2 -0.45
  ]
  edge
  [
    source 10
    target 18
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 10
    target 19
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 10
    target 29
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 10
    target 31
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 11
    target 0
    weight 0.171
    weight2 -0.414
  ]
  edge
  [
    source 11
    target 6
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 11
    target 7
    weight 0.275
    weight2 -0.524
  ]
  edge
  [
    source 11
    target 16
    weight 0.186
    weight2 -0.431
  ]
  edge
  [
    source 11
    target 18
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 11
    target 19
    weight 0.215
    weight2 -0.463
  ]
  edge
  [
    source 11
    target 23
    weight 0.226
    weight2 -0.475
  ]
  edge
  [
    source 11
    target 29
    weight 0.161
    weight2 -0.401
  ]
  edge
  [
    source 11
    target 31
    weight 0.223
    weight2 -0.472
  ]
  edge
  [
    source 12
    target 13
    weight 0.343
    weight2 0.585
  ]
  edge
  [
    source 13
    target 12
    weight 0.343
    weight2 0.585
  ]
  edge
  [
    source 14
    target 2
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 14
    target 3
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 14
    target 4
    weight 0.299
    weight2 -0.547
  ]
  edge
  [
    source 14
    target 5
    weight 0.215
    weight2 -0.463
  ]
  edge
  [
    source 14
    target 18
    weight 0.21
    weight2 -0.458
  ]
  edge
  [
    source 14
    target 22
    weight 0.214
    weight2 -0.462
  ]
  edge
  [
    source 14
    target 27
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 15
    target 0
    weight 0.212
    weight2 -0.46
  ]
  edge
  [
    source 15
    target 1
    weight 0.285
    weight2 -0.534
  ]
  edge
  [
    source 16
    target 10
    weight 0.203
    weight2 -0.45
  ]
  edge
  [
    source 16
    target 11
    weight 0.186
    weight2 -0.431
  ]
  edge
  [
    source 16
    target 18
    weight 0.301
    weight2 -0.549
  ]
  edge
  [
    source 16
    target 24
    weight 0.308
    weight2 -0.555
  ]
  edge
  [
    source 17
    target 0
    weight 0.207
    weight2 -0.455
  ]
  edge
  [
    source 17
    target 6
    weight 0.32
    weight2 -0.566
  ]
  edge
  [
    source 17
    target 7
    weight 0.234
    weight2 -0.484
  ]
  edge
  [
    source 17
    target 18
    weight 0.225
    weight2 -0.474
  ]
  edge
  [
    source 17
    target 23
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 17
    target 24
    weight 0.281
    weight2 -0.53
  ]
  edge
  [
    source 17
    target 31
    weight 0.245
    weight2 -0.495
  ]
  edge
  [
    source 18
    target 1
    weight 0.19
    weight2 -0.436
  ]
  edge
  [
    source 18
    target 6
    weight 0.278
    weight2 -0.527
  ]
  edge
  [
    source 18
    target 10
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 18
    target 11
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 18
    target 14
    weight 0.21
    weight2 -0.458
  ]
  edge
  [
    source 18
    target 16
    weight 0.301
    weight2 -0.549
  ]
  edge
  [
    source 18
    target 17
    weight 0.225
    weight2 -0.474
  ]
  edge
  [
    source 18
    target 22
    weight 0.196
    weight2 -0.442
  ]
  edge
  [
    source 18
    target 31
    weight 0.161
    weight2 -0.401
  ]
  edge
  [
    source 19
    target 1
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 19
    target 2
    weight 0.167
    weight2 -0.409
  ]
  edge
  [
    source 19
    target 6
    weight 0.309
    weight2 -0.556
  ]
  edge
  [
    source 19
    target 7
    weight 0.231
    weight2 -0.48
  ]
  edge
  [
    source 19
    target 10
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 19
    target 11
    weight 0.215
    weight2 -0.463
  ]
  edge
  [
    source 19
    target 24
    weight 0.425
    weight2 -0.652
  ]
  edge
  [
    source 19
    target 31
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 20
    target 4
    weight 0.26
    weight2 -0.51
  ]
  edge
  [
    source 20
    target 5
    weight 0.385
    weight2 -0.62
  ]
  edge
  [
    source 20
    target 21
    weight 0.441
    weight2 0.664
  ]
  edge
  [
    source 20
    target 25
    weight 0.326
    weight2 -0.571
  ]
  edge
  [
    source 20
    target 26
    weight 0.304
    weight2 -0.551
  ]
  edge
  [
    source 20
    target 32
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 20
    target 33
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 21
    target 4
    weight 0.241
    weight2 -0.491
  ]
  edge
  [
    source 21
    target 5
    weight 0.407
    weight2 -0.638
  ]
  edge
  [
    source 21
    target 20
    weight 0.441
    weight2 0.664
  ]
  edge
  [
    source 21
    target 25
    weight 0.31
    weight2 -0.557
  ]
  edge
  [
    source 21
    target 26
    weight 0.395
    weight2 -0.628
  ]
  edge
  [
    source 21
    target 28
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 21
    target 32
    weight 0.199
    weight2 -0.447
  ]
  edge
  [
    source 22
    target 2
    weight 0.219
    weight2 -0.468
  ]
  edge
  [
    source 22
    target 3
    weight 0.26
    weight2 -0.51
  ]
  edge
  [
    source 22
    target 14
    weight 0.214
    weight2 -0.462
  ]
  edge
  [
    source 22
    target 18
    weight 0.196
    weight2 -0.442
  ]
  edge
  [
    source 23
    target 0
    weight 0.266
    weight2 -0.515
  ]
  edge
  [
    source 23
    target 11
    weight 0.226
    weight2 -0.475
  ]
  edge
  [
    source 23
    target 17
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 23
    target 29
    weight 0.23
    weight2 -0.479
  ]
  edge
  [
    source 24
    target 2
    weight 0.174
    weight2 -0.418
  ]
  edge
  [
    source 24
    target 6
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 24
    target 16
    weight 0.308
    weight2 -0.555
  ]
  edge
  [
    source 24
    target 17
    weight 0.281
    weight2 -0.53
  ]
  edge
  [
    source 24
    target 19
    weight 0.425
    weight2 -0.652
  ]
  edge
  [
    source 24
    target 29
    weight 0.167
    weight2 -0.408
  ]
  edge
  [
    source 24
    target 30
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 24
    target 31
    weight 0.346
    weight2 -0.588
  ]
  edge
  [
    source 25
    target 20
    weight 0.326
    weight2 -0.571
  ]
  edge
  [
    source 25
    target 21
    weight 0.31
    weight2 -0.557
  ]
  edge
  [
    source 26
    target 20
    weight 0.304
    weight2 -0.551
  ]
  edge
  [
    source 26
    target 21
    weight 0.395
    weight2 -0.628
  ]
  edge
  [
    source 26
    target 28
    weight 0.179
    weight2 -0.424
  ]
  edge
  [
    source 27
    target 14
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 28
    target 21
    weight 0.209
    weight2 -0.457
  ]
  edge
  [
    source 28
    target 26
    weight 0.179
    weight2 -0.424
  ]
  edge
  [
    source 29
    target 1
    weight 0.168
    weight2 -0.41
  ]
  edge
  [
    source 29
    target 10
    weight 0.201
    weight2 -0.448
  ]
  edge
  [
    source 29
    target 11
    weight 0.161
    weight2 -0.401
  ]
  edge
  [
    source 29
    target 23
    weight 0.23
    weight2 -0.479
  ]
  edge
  [
    source 29
    target 24
    weight 0.167
    weight2 -0.408
  ]
  edge
  [
    source 30
    target 0
    weight 0.269
    weight2 -0.519
  ]
  edge
  [
    source 30
    target 24
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 31
    target 1
    weight 0.24
    weight2 -0.489
  ]
  edge
  [
    source 31
    target 4
    weight 0.177
    weight2 -0.421
  ]
  edge
  [
    source 31
    target 6
    weight 0.276
    weight2 -0.525
  ]
  edge
  [
    source 31
    target 7
    weight 0.269
    weight2 -0.518
  ]
  edge
  [
    source 31
    target 10
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 31
    target 11
    weight 0.223
    weight2 -0.472
  ]
  edge
  [
    source 31
    target 17
    weight 0.245
    weight2 -0.495
  ]
  edge
  [
    source 31
    target 18
    weight 0.161
    weight2 -0.401
  ]
  edge
  [
    source 31
    target 19
    weight 0.161
    weight2 -0.402
  ]
  edge
  [
    source 31
    target 24
    weight 0.346
    weight2 -0.588
  ]
  edge
  [
    source 32
    target 5
    weight 0.221
    weight2 -0.47
  ]
  edge
  [
    source 32
    target 20
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 32
    target 21
    weight 0.199
    weight2 -0.447
  ]
  edge
  [
    source 32
    target 33
    weight 0.616
    weight2 0.785
  ]
  edge
  [
    source 33
    target 5
    weight 0.193
    weight2 -0.439
  ]
  edge
  [
    source 33
    target 20
    weight 0.179
    weight2 -0.423
  ]
  edge
  [
    source 33
    target 32
    weight 0.616
    weight2 0.785
  ]
]
