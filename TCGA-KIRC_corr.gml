Creator "igraph version 1.2.2 Wed Jul 24 12:20:26 2019"
Version 1
graph
[
  directed 1
  node
  [
    id 0
    name "c.1p"
  ]
  node
  [
    id 1
    name "c.1q"
  ]
  node
  [
    id 2
    name "c.2p"
  ]
  node
  [
    id 3
    name "c.2q"
  ]
  node
  [
    id 4
    name "c.3p"
  ]
  node
  [
    id 5
    name "c.4p"
  ]
  node
  [
    id 6
    name "c.4q"
  ]
  node
  [
    id 7
    name "c.6p"
  ]
  node
  [
    id 8
    name "c.6q"
  ]
  node
  [
    id 9
    name "c.7p"
  ]
  node
  [
    id 10
    name "c.7q"
  ]
  node
  [
    id 11
    name "c.8q"
  ]
  node
  [
    id 12
    name "c.9p"
  ]
  node
  [
    id 13
    name "c.9q"
  ]
  node
  [
    id 14
    name "c.10p"
  ]
  node
  [
    id 15
    name "c.10q"
  ]
  node
  [
    id 16
    name "c.11p"
  ]
  node
  [
    id 17
    name "c.11q"
  ]
  node
  [
    id 18
    name "c.12p"
  ]
  node
  [
    id 19
    name "c.12q"
  ]
  node
  [
    id 20
    name "c.13q"
  ]
  node
  [
    id 21
    name "c.14q"
  ]
  node
  [
    id 22
    name "c.15q"
  ]
  node
  [
    id 23
    name "c.17p"
  ]
  node
  [
    id 24
    name "c.17q"
  ]
  node
  [
    id 25
    name "c.19q"
  ]
  edge
  [
    source 0
    target 4
    weight 0.182
    weight2 -0.426
  ]
  edge
  [
    source 0
    target 5
    weight 0.24
    weight2 -0.49
  ]
  edge
  [
    source 0
    target 6
    weight 0.266
    weight2 -0.515
  ]
  edge
  [
    source 0
    target 7
    weight 0.256
    weight2 -0.506
  ]
  edge
  [
    source 0
    target 8
    weight 0.191
    weight2 -0.438
  ]
  edge
  [
    source 0
    target 12
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 0
    target 14
    weight 0.208
    weight2 -0.456
  ]
  edge
  [
    source 0
    target 21
    weight 0.2
    weight2 -0.448
  ]
  edge
  [
    source 1
    target 21
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 2
    target 4
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 2
    target 6
    weight 0.195
    weight2 -0.442
  ]
  edge
  [
    source 2
    target 16
    weight 0.192
    weight2 -0.438
  ]
  edge
  [
    source 2
    target 17
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 2
    target 22
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 2
    target 25
    weight 0.25
    weight2 -0.5
  ]
  edge
  [
    source 3
    target 4
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 3
    target 6
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 3
    target 16
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 3
    target 17
    weight 0.18
    weight2 -0.424
  ]
  edge
  [
    source 3
    target 22
    weight 0.164
    weight2 -0.405
  ]
  edge
  [
    source 3
    target 25
    weight 0.232
    weight2 -0.481
  ]
  edge
  [
    source 4
    target 0
    weight 0.182
    weight2 -0.426
  ]
  edge
  [
    source 4
    target 2
    weight 0.165
    weight2 -0.406
  ]
  edge
  [
    source 4
    target 3
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 5
    target 0
    weight 0.24
    weight2 -0.49
  ]
  edge
  [
    source 5
    target 7
    weight 0.312
    weight2 -0.559
  ]
  edge
  [
    source 5
    target 8
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 5
    target 15
    weight 0.284
    weight2 -0.533
  ]
  edge
  [
    source 5
    target 20
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 5
    target 23
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 5
    target 24
    weight 0.162
    weight2 -0.402
  ]
  edge
  [
    source 6
    target 0
    weight 0.266
    weight2 -0.515
  ]
  edge
  [
    source 6
    target 2
    weight 0.195
    weight2 -0.442
  ]
  edge
  [
    source 6
    target 3
    weight 0.218
    weight2 -0.467
  ]
  edge
  [
    source 6
    target 7
    weight 0.17
    weight2 -0.412
  ]
  edge
  [
    source 6
    target 8
    weight 0.341
    weight2 -0.584
  ]
  edge
  [
    source 6
    target 15
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 6
    target 20
    weight 0.2
    weight2 -0.447
  ]
  edge
  [
    source 7
    target 0
    weight 0.256
    weight2 -0.506
  ]
  edge
  [
    source 7
    target 5
    weight 0.312
    weight2 -0.559
  ]
  edge
  [
    source 7
    target 6
    weight 0.17
    weight2 -0.412
  ]
  edge
  [
    source 7
    target 21
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 7
    target 22
    weight 0.16
    weight2 -0.4
  ]
  edge
  [
    source 8
    target 0
    weight 0.191
    weight2 -0.438
  ]
  edge
  [
    source 8
    target 5
    weight 0.252
    weight2 -0.502
  ]
  edge
  [
    source 8
    target 6
    weight 0.341
    weight2 -0.584
  ]
  edge
  [
    source 8
    target 12
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 8
    target 21
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 9
    target 10
    weight 0.168
    weight2 0.41
  ]
  edge
  [
    source 10
    target 9
    weight 0.168
    weight2 0.41
  ]
  edge
  [
    source 11
    target 17
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 12
    target 0
    weight 0.255
    weight2 -0.505
  ]
  edge
  [
    source 12
    target 8
    weight 0.205
    weight2 -0.452
  ]
  edge
  [
    source 12
    target 13
    weight 0.536
    weight2 0.732
  ]
  edge
  [
    source 12
    target 15
    weight 0.166
    weight2 -0.407
  ]
  edge
  [
    source 13
    target 12
    weight 0.536
    weight2 0.732
  ]
  edge
  [
    source 14
    target 0
    weight 0.208
    weight2 -0.456
  ]
  edge
  [
    source 15
    target 5
    weight 0.284
    weight2 -0.533
  ]
  edge
  [
    source 15
    target 6
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 15
    target 12
    weight 0.166
    weight2 -0.407
  ]
  edge
  [
    source 15
    target 21
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 15
    target 22
    weight 0.16
    weight2 -0.4
  ]
  edge
  [
    source 16
    target 2
    weight 0.192
    weight2 -0.438
  ]
  edge
  [
    source 16
    target 3
    weight 0.237
    weight2 -0.487
  ]
  edge
  [
    source 16
    target 24
    weight 0.186
    weight2 -0.431
  ]
  edge
  [
    source 17
    target 2
    weight 0.216
    weight2 -0.465
  ]
  edge
  [
    source 17
    target 3
    weight 0.18
    weight2 -0.424
  ]
  edge
  [
    source 17
    target 11
    weight 0.188
    weight2 -0.434
  ]
  edge
  [
    source 17
    target 24
    weight 0.24
    weight2 -0.49
  ]
  edge
  [
    source 18
    target 19
    weight 0.206
    weight2 0.454
  ]
  edge
  [
    source 19
    target 18
    weight 0.206
    weight2 0.454
  ]
  edge
  [
    source 20
    target 5
    weight 0.196
    weight2 -0.443
  ]
  edge
  [
    source 20
    target 6
    weight 0.2
    weight2 -0.447
  ]
  edge
  [
    source 21
    target 0
    weight 0.2
    weight2 -0.448
  ]
  edge
  [
    source 21
    target 1
    weight 0.172
    weight2 -0.415
  ]
  edge
  [
    source 21
    target 7
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 21
    target 8
    weight 0.249
    weight2 -0.499
  ]
  edge
  [
    source 21
    target 15
    weight 0.332
    weight2 -0.576
  ]
  edge
  [
    source 22
    target 2
    weight 0.187
    weight2 -0.433
  ]
  edge
  [
    source 22
    target 3
    weight 0.164
    weight2 -0.405
  ]
  edge
  [
    source 22
    target 7
    weight 0.16
    weight2 -0.4
  ]
  edge
  [
    source 22
    target 15
    weight 0.16
    weight2 -0.4
  ]
  edge
  [
    source 22
    target 24
    weight 0.22
    weight2 -0.469
  ]
  edge
  [
    source 23
    target 5
    weight 0.163
    weight2 -0.404
  ]
  edge
  [
    source 24
    target 5
    weight 0.162
    weight2 -0.402
  ]
  edge
  [
    source 24
    target 16
    weight 0.186
    weight2 -0.431
  ]
  edge
  [
    source 24
    target 17
    weight 0.24
    weight2 -0.49
  ]
  edge
  [
    source 24
    target 22
    weight 0.22
    weight2 -0.469
  ]
  edge
  [
    source 24
    target 25
    weight 0.3
    weight2 -0.548
  ]
  edge
  [
    source 25
    target 2
    weight 0.25
    weight2 -0.5
  ]
  edge
  [
    source 25
    target 3
    weight 0.232
    weight2 -0.481
  ]
  edge
  [
    source 25
    target 24
    weight 0.3
    weight2 -0.548
  ]
]
